package com.onurzubari.games.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.onurzubari.games.IntoTheVoid;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Into the Void";

		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
        config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;

		/*
		config.stageWidth = 1280;
		config.stageHeight = 768;
		config.vSyncEnabled = false;  //Disables FPS throttling.
		config.foregroundFPS = 400;
		config.backgroundFPS = 400;
		*/

		config.backgroundFPS = 10;
		config.resizable = false;
		config.fullscreen = false;
		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		new LwjglApplication(new IntoTheVoid(), config);
	}
}