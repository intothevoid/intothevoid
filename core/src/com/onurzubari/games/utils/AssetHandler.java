package com.onurzubari.games.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Zubari on 14.06.2017.
 * Into the void.
 */
public class AssetHandler {

    public AssetManager manager;

    public AssetHandler(){
        manager = new com.badlogic.gdx.assets.AssetManager();
        manager.load("bullet.png", Texture.class);
        manager.finishLoading();

    }

    public AssetManager getAssetManager() {
        return manager;
    }
}
