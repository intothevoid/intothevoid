package com.onurzubari.games.utils;

import com.bitfire.postprocessing.effects.Zoomer;

public class JumpAnimation {
	float timer = 2, time = 0, blurStrength;
	Zoomer zoomer;
	
	public void update(Zoomer zoomer, float delta, boolean jumping){
		blurStrength = zoomer.getBlurStrength();
		if(timer > time && jumping == true){
			time += delta;
			zoomer.setBlurStrength(blurStrength + 0.01f);
		} else {
		    time = 0;
        }
	}

    public void jumpingContinue(Zoomer zoomer, float delta, boolean jumping){
        blurStrength = zoomer.getBlurStrength();
        if(timer > time && jumping == true){
            time += delta;
            zoomer.setBlurStrength(blurStrength - 0.01f);
        } else {
            zoomer.setBlurStrength(0);
            time = 0;
        }
    }

	public boolean updateStatus(){
		if(time != 0){
			return true;
		}
		return false;
	}
}
