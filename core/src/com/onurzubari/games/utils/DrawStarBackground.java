package com.onurzubari.games.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.onurzubari.games.IntoTheVoid;

import java.util.ArrayList;

public class DrawStarBackground {
	
	Stage stage;
	public float regionWidth, regionHeight;
	public Texture starTexture;
	public static Texture BackgroundTextTexture;
	static Sprite BackgroundTextSprite;
	TextureRegion starTextureRegion;
	TextureRegionDrawable starTextureRegionDrawable;
    ArrayList<Image> images;
	
	public DrawStarBackground(Stage stage, float regionWidth, float regionHeight){
		this.stage = stage;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
		starTexture = new Texture(Gdx.files.internal("star.jpg"));
		starTexture.getUWrap();
		starTexture.getVWrap();
		starTexture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		starTextureRegion = new TextureRegion(starTexture);
		starTextureRegion.setRegion(0, 0, starTexture.getWidth() * 2, starTexture.getHeight() * 2);
		starTextureRegionDrawable = new TextureRegionDrawable(starTextureRegion);

        images = new ArrayList<>(9);

		for(int i = 0; i<9; i++){
            images.add(new Image());
            images.get(i).setSize(regionWidth, regionHeight);
            images.get(i).setDrawable(starTextureRegionDrawable);
            stage.addActor(images.get(i));
        }
        images.get(0).setPosition(0,0);
	}

	public void repeatBackground(float rightFrame, float leftFrame, float upFrame, float downFrame){
		if(rightFrame > images.get(0).getX()+regionWidth){
            images.get(0).setX(images.get(0).getX()+regionWidth);
            setRelativePositions();
		}

		if(leftFrame < images.get(0).getX()){
            images.get(0).setX(images.get(0).getX()-regionWidth);
            setRelativePositions();
        }

        if(upFrame > images.get(0).getY()+regionHeight){
            images.get(0).setY(images.get(0).getY()+regionHeight);
            setRelativePositions();
        }

        if(downFrame < images.get(0).getY()) {
            images.get(0).setY(images.get(0).getY()-regionHeight);
            setRelativePositions();
        }
	}

	private void setRelativePositions(){
        images.get(1).setPosition(images.get(0).getX(),images.get(0).getY()+regionHeight);
        images.get(2).setPosition(images.get(0).getX()+regionWidth,images.get(0).getY()+regionHeight);
        images.get(3).setPosition(images.get(0).getX()+regionWidth,images.get(0).getY());
        images.get(4).setPosition(images.get(0).getX()+regionWidth,images.get(0).getY()-regionHeight);
        images.get(5).setPosition(images.get(0).getX(),images.get(0).getY()-regionHeight);
        images.get(6).setPosition(images.get(0).getX()-regionWidth,images.get(0).getY()-regionHeight);
        images.get(7).setPosition(images.get(0).getX()-regionWidth,images.get(0).getY());
        images.get(8).setPosition(images.get(0).getX()-regionWidth,images.get(0).getY()-regionHeight);
    }

	public void setBackgroundTextTexture(String filename){
		BackgroundTextTexture = new Texture(Gdx.files.internal(filename));
		BackgroundTextSprite = new Sprite(BackgroundTextTexture, 0, 0, BackgroundTextTexture.getWidth(), BackgroundTextTexture.getHeight());
		BackgroundTextSprite.setBounds(-IntoTheVoid.windowWidth/20, -IntoTheVoid.windowHeight/20, IntoTheVoid.windowWidth/10, IntoTheVoid.windowHeight/10);
	}

	public void drawBackgroundText(Batch batch){
		BackgroundTextSprite.draw(batch);
	}
}
