package com.onurzubari.games.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.onurzubari.games.utils.LRUCache.CacheEntryRemovedListener;

/**
 * A service that manages the sound effects.
 */
public class SoundHandler implements CacheEntryRemovedListener<SoundHandler.ITVSound, Sound>, Disposable {
    /**
     * The available sound files.
     */
    public enum ITVSound {
        Shot("sound/shot.mp3"),
        Explosion2("sound/explosion2.mp3");

        private final String fileName;

        private ITVSound(String fileName) {
            this.fileName = fileName;
        }

        public String getFileName() {
            return fileName;
        }
    }

    /**
     * The volume to be set on the sound.
     */
    private float volume = 0.01f;

    /**
     * Whether the sound is enabled.
     */
    private boolean enabled = true;

    /**
     * The sound cache.
     */
    private final LRUCache<ITVSound, Sound> soundCache;

    /**
     * Creates the sound manager.
     */
    public SoundHandler() {
        soundCache = new LRUCache<ITVSound, Sound>(10);
        soundCache.setEntryRemovedListener(this);
    }

    /**
     * Plays the specified sound.
     */
    public void play(ITVSound sound) {
        // check if the sound is enabled
        if (!enabled) return;

        // try and get the sound from the cache
        Sound soundToPlay = soundCache.get(sound);
        if (soundToPlay == null) {
            FileHandle soundFile = Gdx.files.internal(sound.getFileName());
            soundToPlay = Gdx.audio.newSound(soundFile);
            soundCache.add(sound, soundToPlay);
        }

        // play the sound
        //Gdx.app.log("Log:", "Playing sound: " + sound.name());
        soundToPlay.play(volume);
    }

    /**
     * Sets the sound volume which must be inside the range [0,1].
     */
    public void setVolume(float volume) {
        Gdx.app.log("Log:", "Adjusting sound volume to: " + volume);

        // check and set the new volume
        if (volume < 0 || volume > 1f) {
            throw new IllegalArgumentException("The volume must be inside the range: [0,1]");
        }
        this.volume = volume;
    }

    /**
     * Enables or disabled the sound.
     */
    public void setEnabled(
            boolean enabled) {
        this.enabled = enabled;
    }

    // EntryRemovedListener implementation

    @Override
    public void notifyEntryRemoved(
            ITVSound key,
            Sound value) {
        //Gdx.app.log("Log:", "Disposing sound: " + key.name());
        value.dispose();
    }

    /**
     * Disposes the sound manager.
     */
    public void dispose() {
        //Gdx.app.log("Log:", "Disposing sound manager");
        for (Sound sound : soundCache.retrieveAll()) {
            sound.stop();
            sound.dispose();
        }
    }
}
