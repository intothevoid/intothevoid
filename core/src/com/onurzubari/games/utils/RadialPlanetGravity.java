package com.onurzubari.games.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.CircleShape;

import java.util.ArrayList;

public class RadialPlanetGravity {

	public static void apply(ArrayList<Body> planetVector, ArrayList<Body> debrisVector){
		int i, j;
		for (i=0; i<debrisVector.size(); i++) {
			Vector2 debrisPosition = debrisVector.get(i).getWorldCenter();
			for(j=0; j<planetVector.size(); j++){
				CircleShape planetShape = (CircleShape) planetVector.get(j).getFixtureList().first().getShape();
				float planetRadius = planetShape.getRadius();
				Vector2 planetPosition = planetVector.get(j).getWorldCenter();
				Vector2 planetDistance = new Vector2(0,0);
				planetDistance.add(debrisPosition);
				planetDistance.sub(planetPosition);
				float finalDistance = planetDistance.len();
				if (finalDistance <= planetRadius * 2) {
					planetDistance.scl((planetRadius*2-finalDistance)*-0.01f);
					debrisVector.get(i).applyForce(planetDistance, debrisVector.get(i).getWorldCenter(), true);
				}
			}
		}
	}
}