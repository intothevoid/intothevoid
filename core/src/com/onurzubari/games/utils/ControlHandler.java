package com.onurzubari.games.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.objects.Player;

/**
 * Created by Zubari on 5.03.2017.
 */
public class ControlHandler {

    Vector2 velocity, linearVelocity;
    float speed, shipAngle, fuel, angularVelocity;
    boolean fueled, running;
    IntoTheVoid app;
    Player player;

    public ControlHandler(IntoTheVoid app, Player player){
        this.app = app;
        this.player = player;
        this.fuel = player.ship.currentFuel;
    }

    public void handle(float delta){
        float force = 2f;
        fueled = player.ship.currentFuel > 0;
        shipAngle = player.ship.body.getAngle();
        this.app.thrust.pause();

        if (Gdx.input.isKeyPressed(Input.Keys.A) && fueled) {
            player.ship.body.setAngularDamping(0);
            player.ship.body.applyAngularImpulse(force, true);
            fuel -= 0.01;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D) && fueled) {
            player.ship.body.setAngularDamping(0);
            player.ship.body.applyAngularImpulse(-force, true);
            fuel -= 0.01;
        }

        angularVelocity = player.ship.body.getAngularVelocity();
        linearVelocity = player.ship.body.getLinearVelocity();

        if (angularVelocity != 0 && fueled && (!Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.D))) {
            player.ship.body.setAngularDamping(force*5);
            fuel -= 0.005;
            if (angularVelocity < 0.2f && angularVelocity > -0.2f) {
                player.ship.body.setAngularVelocity(0);
                player.ship.body.setAngularDamping(0);
            }
        }

        velocity = player.ship.body.getLinearVelocity();
        speed = velocity.len();
        if(speed > player.ship.maxSpeed){
            player.ship.body.setLinearVelocity(velocity.scl(player.ship.maxSpeed/speed));
        }
        if(Math.abs(player.ship.body.getAngularVelocity()) > player.ship.maxRotationalSpeed){
            player.ship.body.setAngularVelocity(Math.copySign(player.ship.maxRotationalSpeed, player.ship.body.getAngularVelocity()));
        }

        if (Gdx.input.isKeyPressed(Input.Keys.W) && fueled) {
            player.ship.body.applyForce(force * MathUtils.cos(shipAngle), force * MathUtils.sin(shipAngle),
                    player.ship.body.getPosition().x, player.ship.body.getPosition().y, true);
            fuel -= 0.1;
            player.ship.engineRunning = true;
            this.app.thrust.resume(0);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.S) && fueled) {
            player.ship.body.applyForce(-force * MathUtils.cos(shipAngle), -force * MathUtils.sin(shipAngle),
                    player.ship.body.getPosition().x, player.ship.body.getPosition().y, true);
            fuel -= 0.1;
            player.ship.engineRunning = true;
            this.app.thrust.resume(0);
        }

        if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            if(player.ship.reloadTimer <= 0){
                player.shoot();
                player.ship.body.applyForce(MathUtils.cos(shipAngle)*-force,MathUtils.sin(shipAngle)*-force, player.ship.body.getPosition().x, player.ship.body.getPosition().y, true);
                player.ship.reloadTimer = player.ship.reloadTime;
            }
        }
        if(player.ship.reloadTimer > 0){
            player.ship.reloadTimer -= delta;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            player.ship.body.applyLinearImpulse(new Vector2(0,0).sub(velocity.scl(1/60f)), player.ship.body.getWorldCenter(), true);
            fuel = player.ship.fuelCapacity;
            if (player.ship.body.getLinearVelocity().x < 0.05f && player.ship.body.getLinearVelocity().x > -0.05f) {
                player.ship.body.setLinearVelocity(0, player.ship.body.getLinearVelocity().y);
            }
            if (player.ship.body.getLinearVelocity().y < 0.05f && player.ship.body.getLinearVelocity().y > -0.05f) {
                player.ship.body.setLinearVelocity(player.ship.body.getLinearVelocity().x, 0);
            }
        }
        player.ship.currentFuel = fuel;
    }

    public boolean getRunning(){
        return  running;
    }

    public void setRunning(boolean running){
        this.running = running;
    }
}
