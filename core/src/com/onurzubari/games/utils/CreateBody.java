package com.onurzubari.games.utils;

import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.onurzubari.games.objects.Space;

public class CreateBody {
	
	public static Body createPlanet(Space space, float x, float y, float radius) {
		BodyDef nodeBodyDefinition = new BodyDef();
		nodeBodyDefinition.type = BodyType.StaticBody;

		CircleShape shape = new CircleShape();
		float density = 1.0f;
		shape.setRadius(radius);

		Body body = space.world.createBody(nodeBodyDefinition);
		body.setTransform(x, y, 0);
		final FixtureDef nodeFixtureDefinition = FixtureDefinition.create(shape, density);

		body.createFixture(nodeFixtureDefinition);
		shape.dispose();
		space.planetVector.add(body);
		return body;
	}
	
	public static Body createStar(World world, float x, float y, float radius) {
		BodyDef nodeBodyDefinition = new BodyDef();
		nodeBodyDefinition.type = BodyType.StaticBody;

		CircleShape shape = new CircleShape();
		float density = 1.0f;
		shape.setRadius(radius);

		Body body = world.createBody(nodeBodyDefinition);
		body.setTransform(x, y, 0);
		final FixtureDef nodeFixtureDefinition = FixtureDefinition.create(shape, density);

		body.createFixture(nodeFixtureDefinition);
		shape.dispose();
		return body;
	}
	
	public static Body createBox(Space space, float w, float h, float x, float y) {
		BodyDef nodeBodyDefinition = new BodyDef();
		nodeBodyDefinition.type = BodyDef.BodyType.DynamicBody;

		PolygonShape shape = new PolygonShape();
		float density = 2.0f;
		shape.setAsBox(w, h);

		Body body = space.world.createBody(nodeBodyDefinition);
		body.setTransform(x, y, 0);
		final FixtureDef nodeFixtureDefinition = FixtureDefinition.create(shape, density);

		body.createFixture(nodeFixtureDefinition);
		shape.dispose();
		space.debrisVector.add(body);
		return body;
	}

}
