package com.onurzubari.games.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Zubari on 19.03.2017.
 */
public class PlanetFocus {
    Texture focusTexture;
    Sprite focusSprite;

    Texture focusTexture2;
    Sprite focusSprite2;

    Texture focusTexture3;
    Sprite focusSprite3;

    SpriteBatch batch;

    public PlanetFocus(SpriteBatch batch){
        this.batch = batch;
        focusTexture = new Texture(Gdx.files.internal("1.png"));
        focusTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        focusSprite = new Sprite(focusTexture, 0, 0, focusTexture.getWidth(), focusTexture.getHeight());
        focusSprite.setOrigin(focusTexture.getWidth()/2, focusTexture.getHeight()/2);

        focusTexture2 = new Texture(Gdx.files.internal("2.png"));
        focusTexture2.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        focusSprite2 = new Sprite(focusTexture2, 0, 0, focusTexture2.getWidth(), focusTexture2.getHeight());
        focusSprite2.setOrigin(focusTexture2.getWidth()/2, focusTexture2.getHeight()/2);

        focusTexture3 = new Texture(Gdx.files.internal("3.png"));
        focusTexture3.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        focusSprite3 = new Sprite(focusTexture3, 0, 0, focusTexture3.getWidth(), focusTexture3.getHeight());
        focusSprite3.setOrigin(focusTexture3.getWidth()/2, focusTexture3.getHeight()/2);
    }

    public void draw(float x, float y, float radius){
        focusSprite.setPosition(x - focusTexture.getWidth()/2,y - focusTexture.getHeight()/2);
        focusSprite.setScale(radius/240);
        focusSprite.rotate(0.3f);
        focusSprite.draw(batch);
        focusSprite2.setPosition(x - focusTexture2.getWidth()/2,y - focusTexture2.getHeight()/2);
        focusSprite2.setScale(radius/240);
        focusSprite2.rotate(-0.2f);
        focusSprite2.draw(batch);
        focusSprite3.setPosition(x - focusTexture3.getWidth()/2,y - focusTexture3.getHeight()/2);
        focusSprite3.setScale(radius/222);
        focusSprite3.rotate(0.1f);
        focusSprite3.draw(batch);
    }
}
