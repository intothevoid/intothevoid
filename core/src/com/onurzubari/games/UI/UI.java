package com.onurzubari.games.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.onurzubari.games.objects.NPC;
import com.onurzubari.games.objects.Player;

import java.util.ArrayList;

public class UI {
    private SpriteBatch batch;
    private ArrayList<Sprite> fuelBarArray, shieldBarArray, shieldBarArrayEnemy, hullBarArray, hullBarArrayEnemy, expBarArray;
    public NPC npc;

	private Sprite topRightCorner, expBar, middle, enemy, smallIndicator, smallIndicatorHull, smallIndicatorShield;
	public UI(SpriteBatch batch){
	    this.batch = batch;

        Texture topRightCornerTexture = new Texture(Gdx.files.internal("topRightCorner.png"));
        topRightCorner = new Sprite(topRightCornerTexture);
        topRightCorner.setPosition(Gdx.graphics.getWidth() - topRightCorner.getWidth(), Gdx.graphics.getHeight() - topRightCorner.getHeight());

        Texture expBarTexture = new Texture(Gdx.files.internal("experimental.png"));
        expBar = new Sprite(expBarTexture);
        expBar.setPosition(Gdx.graphics.getWidth()/2 - expBar.getWidth()/2, 0);

        Texture middleTexture = new Texture(Gdx.files.internal("middle.png"));
        middle = new Sprite(middleTexture);
        middle.setPosition(Gdx.graphics.getWidth()/2 - middle.getWidth()/2, expBar.getHeight() + 1);

        Texture enemyMiddleTexture = new Texture(Gdx.files.internal("enemymiddle.png"));
        enemy = new Sprite(enemyMiddleTexture);
        enemy.setPosition(Gdx.graphics.getWidth()/2 - middle.getWidth()/2, expBar.getHeight() + 1);

        this.fuelBarArray = new ArrayList<>();
        this.shieldBarArray = new ArrayList<>();
        this.hullBarArray = new ArrayList<>();

        this.shieldBarArrayEnemy = new ArrayList<>();
        this.hullBarArrayEnemy = new ArrayList<>();

        this.expBarArray = new ArrayList<>();

        Texture fuel = new Texture(Gdx.files.internal("fuel.png"));
        Texture shield = new Texture(Gdx.files.internal("shield.png"));
        Texture hull = new Texture(Gdx.files.internal("hull.png"));
        Texture exp = new Texture(Gdx.files.internal("expInner.png"));

        for (int i=0; i<100; i++){
            fuelBarArray.add(i, new Sprite(fuel, 0, 0, fuel.getWidth(), fuel.getHeight()));
            fuelBarArray.get(i).setOrigin(0, 0);
            fuelBarArray.get(i).setPosition(middle.getX() + 238 - (i*2), middle.getY() + 14);

            shieldBarArray.add(i, new Sprite(shield, 0, 0, shield.getWidth(), shield.getHeight()));
            shieldBarArray.get(i).setOrigin(0, 0);
            shieldBarArray.get(i).setPosition(middle.getX() + 238 - (i*2), middle.getY() + 41);

            shieldBarArrayEnemy.add(i, new Sprite(shield, 0, 0, shield.getWidth(), shield.getHeight()));
            shieldBarArrayEnemy.get(i).setOrigin(0, 0);
            shieldBarArrayEnemy.get(i).setPosition(enemy.getX() + 361 + (i*2), enemy.getY() + 41);

            hullBarArray.add(i, new Sprite(hull, 0, 0, hull.getWidth(), hull.getHeight()));
            hullBarArray.get(i).setOrigin(0, 0);
            hullBarArray.get(i).setPosition(middle.getX() + 238 - (i*2), middle.getY() + 24);

            hullBarArrayEnemy.add(i, new Sprite(hull, 0, 0, hull.getWidth(), hull.getHeight()));
            hullBarArrayEnemy.get(i).setOrigin(0, 0);
            hullBarArrayEnemy.get(i).setPosition(enemy.getX() + 361 + (i*2), enemy.getY() + 24);

            expBarArray.add(i, new Sprite(exp, 0, 0, exp.getWidth(), exp.getHeight()));
            expBarArray.get(i).setOrigin(0, 0);
            expBarArray.get(i).setPosition(expBar.getX() + 10 + (i*19), expBar.getY() + 3);
        }

        Texture smallIndicatorTexture = new Texture(Gdx.files.internal("smallIndicator.png"));
        smallIndicator = new Sprite(smallIndicatorTexture);

        Texture smallIndicatorHullTexture = new Texture(Gdx.files.internal("smallIndicatorHull.png"));
        smallIndicatorHull = new Sprite(smallIndicatorHullTexture);

        Texture smallIndicatorShieldTexture = new Texture(Gdx.files.internal("smallIndicatorShield.png"));
        smallIndicatorShield = new Sprite(smallIndicatorShieldTexture);
	}

	public void setSelectedNPC(NPC npc){
	    this.npc = npc;
    }

	public void drawUI(Player player){
        topRightCorner.draw(batch);
        expBar.draw(batch);
        middle.draw(batch);

        int fuel = (int) Math.ceil((player.ship.currentFuel/player.ship.fuelCapacity)*100);
        fill(fuel, batch, fuelBarArray);

        int hull = (int) Math.ceil((player.ship.currentHull/player.ship.hull)*100);
        fill(hull, batch, hullBarArray);

        int shield = (int) Math.ceil((player.ship.currentShield/player.ship.shield)*100);
        fill(shield, batch, shieldBarArray);

        int exp = (int) Math.ceil((player.exp/player.nextLevelExp)*100);
        fill(exp, batch, expBarArray);
	}

    private void fill(int value, SpriteBatch batch, ArrayList<Sprite> array){
        for (int i=0; i<value; i++){
            array.get(i).draw(batch);
        }
    }

    public void drawEnemyUI(){
	    if(npc.alive){
            enemy.draw(batch);
            int hull = (int) Math.ceil((npc.currentHull/npc.hull)*100);
            fill(hull, batch, hullBarArrayEnemy);

            int shield = (int) Math.ceil((npc.currentShield/npc.shield)*100);
            fill(shield, batch, shieldBarArrayEnemy);
        }
    }

    public void drawEnemySmallIndicator(Sprite npc, SpriteBatch batch, float hull, float shield){
            smallIndicator.setSize(npc.getWidth()* 2, npc.getWidth() / 10);
            smallIndicator.setPosition(npc.getX() - smallIndicator.getWidth()/2 + npc.getWidth()/2, npc.getY() + npc.getHeight() + 0.3f);

            smallIndicatorHull.setSize(npc.getWidth()* 2, npc.getWidth() / 10);
            smallIndicatorHull.setPosition(npc.getX() - smallIndicatorHull.getWidth()/2 + npc.getWidth()/2, npc.getY() + npc.getHeight() + 0.3f);

            smallIndicatorShield.setSize(npc.getWidth()* 2, npc.getWidth() / 10);
            smallIndicatorShield.setPosition(npc.getX() - smallIndicatorShield.getWidth()/2 + npc.getWidth()/2, npc.getY() + npc.getHeight() + 0.3f);

            batch.draw(
                    smallIndicator.getTexture(),
                    smallIndicator.getX(),
                    smallIndicator.getY(),
                    smallIndicator.getWidth() / 2,
                    smallIndicator.getHeight() / 2,
                    smallIndicator.getWidth(),
                    smallIndicator.getHeight(),
                    smallIndicator.getScaleX(),
                    smallIndicator.getScaleY(),
                    0 ,
                    smallIndicator.getRegionX(),
                    smallIndicator.getRegionY(),
                    smallIndicator.getRegionWidth(),
                    smallIndicator.getRegionHeight(),
                    false,
                    false);

            batch.draw(
                    smallIndicatorHull.getTexture(),
                    smallIndicatorHull.getX(),
                    smallIndicatorHull.getY(),
                    smallIndicatorHull.getWidth() / 2,
                    smallIndicatorHull.getHeight() / 2,
                    smallIndicatorHull.getWidth() * hull,
                    smallIndicatorHull.getHeight(),
                    smallIndicatorHull.getScaleX(),
                    smallIndicatorHull.getScaleY(),
                    0 ,
                    smallIndicatorHull.getRegionX(),
                    smallIndicatorHull.getRegionY(),
                    (int)Math.ceil(smallIndicatorHull.getRegionWidth() * hull),
                    smallIndicatorHull.getRegionHeight(),
                    false,
                    false);

            batch.draw(
                    smallIndicatorShield.getTexture(),
                    smallIndicatorShield.getX(),
                    smallIndicatorShield.getY(),
                    smallIndicatorShield.getWidth() / 2,
                    smallIndicatorShield.getHeight() / 2,
                    smallIndicatorShield.getWidth() * shield,
                    smallIndicatorShield.getHeight(),
                    smallIndicatorShield.getScaleX(),
                    smallIndicatorShield.getScaleY(),
                    0 ,
                    smallIndicatorShield.getRegionX(),
                    smallIndicatorShield.getRegionY(),
                    (int)Math.ceil(smallIndicatorShield.getRegionWidth() * shield),
                    smallIndicatorShield.getRegionHeight(),
                    false,
                    false);
    }
}
