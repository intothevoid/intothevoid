package com.onurzubari.games.screens;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.*;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.TweenEngine.SpriteAccesor;
import com.onurzubari.games.animations.ButtonAnimation;

import java.util.ArrayList;

public class MainMenu implements Screen {

    private Sprite splashSprite;
    private Sprite buttonHoverSprite;
    private Sprite buttonClickSprite;
    private Sprite dialogBoxSprite;

    private Sprite okayButtonSprite;
    private Sprite cancelButtonSprite;
    private Sprite okayButtonHoverSprite;
    private Sprite cancelButtonHoverSprite;
    private Sprite okayButtonClickedSprite;
    private Sprite cancelButtonClickedSprite;

    private SpriteBatch batch;
    private TweenManager tweenManager;
    private ArrayList<ButtonAnimation> buttonAnimationArray;
    private ArrayList<Sprite> buttonSpriteArray;
    private IntoTheVoid app;
    private Vector3 touchPos;
    private TextField userName, shipName;
    private Stage stage;
    private Boolean dialogVisible = false;
    InputMultiplexer inputMultiplexer;

    public MainMenu(final IntoTheVoid app){
        this.app = app;
        batch = new SpriteBatch();
        tweenManager = new TweenManager();
        buttonAnimationArray = new ArrayList<>();
        buttonSpriteArray = new ArrayList<>();

        stage = new Stage();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Ubuntu-Regular.ttf"));
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = 15;
        BitmapFont ubuntuFont = generator.generateFont(parameter); // font size 12 pixels
        generator.dispose();

        Skin skin = new Skin();
        skin.add("ubuntuFont", ubuntuFont);
        skin.addRegions(new TextureAtlas(Gdx.files.internal("skin/skin.atlas")));
        skin.load(Gdx.files.internal("skin/skin.json"));

        userName = new TextField("Your name", skin);
        shipName = new TextField("Ship name", skin);
        userName.setSize(200, 20);
        shipName.setSize(200,20);

        userName.getStyle().background.setLeftWidth(5f);

        for (int i=0; i<5; i++){
            buttonAnimationArray.add(new ButtonAnimation());
        }

        touchPos = new Vector3();

        Tween.registerAccessor(Sprite.class, new SpriteAccesor());

        Texture splashTexture = new Texture("MainMenu.jpg");
        splashTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        splashSprite = new Sprite(splashTexture);
        splashSprite.setSize(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());

        Texture testingTextTexture = new Texture("buttons/buttonTesting.png");
        Texture newGameTextTexture = new Texture("buttons/buttonNewGame.png");
        Texture loadGameTextTexture = new Texture("buttons/buttonLoadGame.png");
        Texture optionsTextTexture = new Texture("buttons/buttonOptions.png");
        Texture exitTextTexture = new Texture("buttons/buttonExit.png");
        Texture buttonHoverTexture = new Texture("buttons/buttonHover.png");
        Texture buttonClickedTexture = new Texture("buttons/buttonClicked.png");
        Texture dialogBoxTexture = new Texture("dialog.png");
        Texture okayButtonTexture = new Texture("buttons/dialogButtonOkay.png");
        Texture cancelButtonTexture = new Texture("buttons/dialogButtonCancel.png");
        Texture okayButtonHoverTexture = new Texture("buttons/dialogButtonOkayHover.png");
        Texture cancelButtonHoverTexture = new Texture("buttons/dialogButtonCancelHover.png");
        Texture okayButtonClickedTexture = new Texture("buttons/dialogButtonOkayClicked.png");
        Texture cancelButtonClickedTexture = new Texture("buttons/dialogButtonCancelClicked.png");

        testingTextTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        newGameTextTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        loadGameTextTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        optionsTextTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        exitTextTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        buttonHoverTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        buttonClickedTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        dialogBoxTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        okayButtonTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        cancelButtonTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        okayButtonHoverTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        cancelButtonHoverTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        okayButtonClickedTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        cancelButtonClickedTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        buttonHoverSprite = new Sprite(buttonHoverTexture);
        buttonClickSprite = new Sprite(buttonClickedTexture);
        dialogBoxSprite = new Sprite(dialogBoxTexture);
        dialogBoxSprite.setPosition(Gdx.app.getGraphics().getWidth()/2 - dialogBoxSprite.getWidth()/2, Gdx.app.getGraphics().getHeight()/2 - dialogBoxSprite.getHeight()/2);
        userName.setPosition(dialogBoxSprite.getX() + dialogBoxSprite.getWidth()/2 - userName.getWidth()/2, dialogBoxSprite.getY() + dialogBoxSprite.getHeight()/2 + userName.getHeight() * 2);
        shipName.setPosition(dialogBoxSprite.getX() + dialogBoxSprite.getWidth()/2 - shipName.getWidth()/2, dialogBoxSprite.getY() + dialogBoxSprite.getHeight()/2 - (shipName.getHeight()/2));

        okayButtonSprite = new Sprite(okayButtonTexture);
        cancelButtonSprite = new Sprite(cancelButtonTexture);
        okayButtonHoverSprite = new Sprite(okayButtonHoverTexture);
        cancelButtonHoverSprite = new Sprite(cancelButtonHoverTexture);
        okayButtonClickedSprite = new Sprite(okayButtonClickedTexture);
        cancelButtonClickedSprite = new Sprite(cancelButtonClickedTexture);

        okayButtonSprite.setPosition(dialogBoxSprite.getX() + dialogBoxSprite.getWidth()/2 - okayButtonSprite.getWidth()*1.5f, dialogBoxSprite.getY() + okayButtonSprite.getHeight()/2);
        cancelButtonSprite.setPosition(dialogBoxSprite.getX() + dialogBoxSprite.getWidth()/2 + cancelButtonSprite.getWidth()/2, dialogBoxSprite.getY() + cancelButtonSprite.getHeight()/2);

        okayButtonHoverSprite.setPosition(okayButtonSprite.getX(), okayButtonSprite.getY());
        okayButtonClickedSprite.setPosition(okayButtonSprite.getX(), okayButtonSprite.getY());
        cancelButtonHoverSprite.setPosition(cancelButtonSprite.getX(), cancelButtonSprite.getY());
        cancelButtonClickedSprite.setPosition(cancelButtonSprite.getX(), cancelButtonSprite.getY());

        buttonSpriteArray.add(new Sprite(loadGameTextTexture));
        buttonSpriteArray.add(new Sprite(optionsTextTexture));
        buttonSpriteArray.add(new Sprite(newGameTextTexture));
        buttonSpriteArray.add(new Sprite(exitTextTexture));
        buttonSpriteArray.add(new Sprite(testingTextTexture));

        Tween.set(splashSprite, SpriteAccesor.ALPHA).target(0).start(tweenManager);
        Tween.to(splashSprite, SpriteAccesor.ALPHA, 1).target(1).start(tweenManager);

        InputProcessor mouseEvents = new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchUp(int x, int y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    onMouseUp(x, y);
                    return true;
                }
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }

            @Override
            public boolean touchDown(int x, int y, int pointer, int button) {
                stage.setKeyboardFocus(null);
                return false;
            }
        };
        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(mouseEvents);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void show() {
        dialogVisible = false;
        stage.getActors().clear();
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    private void onMouseUp(float x, float y){
        for(Sprite sprite: this.buttonSpriteArray){
            if (sprite.getBoundingRectangle().contains(x, Gdx.app.getGraphics().getHeight()-y)){
                switch (buttonSpriteArray.indexOf(sprite)){
                    case 0:
                        //Load Game
                        break;
                    case 1:
                        //Options
                        app.getMusicManager().stop();
                        break;
                    case 2:
                        //New Game
                        dialogVisible = true;
                        break;
                    case 3:
                        //Exit
                        Gdx.app.exit();
                        break;
                    case 4:
                        //Testing
                        Gdx.input.setInputProcessor(app.solar);
                        app.setScreen(app.solar);
                        break;
                }
            }
        }

        if (okayButtonSprite.getBoundingRectangle().contains(x, Gdx.app.getGraphics().getHeight()-y)){
            app.userName = userName.getText();
            app.shipName = shipName.getText();
            System.out.println("Username = " + app.userName + "\nShipname = " + app.shipName);
            Gdx.input.setInputProcessor(app.solar);
            app.setScreen(app.solar);
        }

        if (cancelButtonSprite.getBoundingRectangle().contains(x, Gdx.app.getGraphics().getHeight()-y)){
            dialogVisible = false;
            stage.getActors().clear();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0,0,0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        tweenManager.update(delta);

        int middleButtonPosX = 60;
        int middleButtonPosY = Gdx.app.getGraphics().getHeight() / 2;

        batch.begin();
        splashSprite.draw(batch);
        for(ButtonAnimation buttonAnimation: this.buttonAnimationArray) {
            switch (buttonAnimationArray.indexOf(buttonAnimation)){
                case 0:
                    buttonAnimation.drawButton(delta, batch, middleButtonPosX, middleButtonPosY);
                    buttonSpriteArray.get(0).setPosition(middleButtonPosX, (middleButtonPosY) -  buttonSpriteArray.get(0).getHeight()/2);
                    buttonSpriteArray.get(0).draw(batch);
                    break;
                case 1:
                    buttonAnimation.drawButton(delta, batch, middleButtonPosX, middleButtonPosY - 100);
                    buttonSpriteArray.get(1).setPosition(middleButtonPosX, (middleButtonPosY - 100) -  buttonSpriteArray.get(1).getHeight()/2);
                    buttonSpriteArray.get(1).draw(batch);
                    break;
                case 2:
                    buttonAnimation.drawButton(delta, batch, middleButtonPosX, middleButtonPosY + 100);
                    buttonSpriteArray.get(2).setPosition(middleButtonPosX, (middleButtonPosY + 100) -  buttonSpriteArray.get(2).getHeight()/2);
                    buttonSpriteArray.get(2).draw(batch);
                    break;
                case 3:
                    buttonAnimation.drawButton(delta, batch, middleButtonPosX, middleButtonPosY - 200);
                    buttonSpriteArray.get(3).setPosition(middleButtonPosX, (middleButtonPosY - 200) -  buttonSpriteArray.get(3).getHeight()/2);
                    buttonSpriteArray.get(3).draw(batch);
                    break;
                case 4:
                    buttonAnimation.drawButton(delta, batch, middleButtonPosX, middleButtonPosY + 200);
                    buttonSpriteArray.get(4).setPosition(middleButtonPosX, (middleButtonPosY + 200) -  buttonSpriteArray.get(4).getHeight()/2);
                    buttonSpriteArray.get(4).draw(batch);
                    break;
            }
        }

        if(dialogVisible){
            stage.addActor(userName);
            stage.addActor(shipName);
            dialogBoxSprite.draw(batch);

            if (okayButtonSprite.getBoundingRectangle().contains(touchPos.x, Gdx.app.getGraphics().getHeight()-touchPos.y)){
                if (Gdx.input.isTouched()){
                    okayButtonClickedSprite.draw(batch);
                }else {
                    okayButtonHoverSprite.draw(batch);
                }
            } else {
                okayButtonSprite.draw(batch);
            }

            if (cancelButtonSprite.getBoundingRectangle().contains(touchPos.x, Gdx.app.getGraphics().getHeight()-touchPos.y)){
                if (Gdx.input.isTouched()){
                    cancelButtonClickedSprite.draw(batch);
                }else {
                    cancelButtonHoverSprite.draw(batch);
                }
            } else {
                cancelButtonSprite.draw(batch);
            }
        }

        touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        for(Sprite sprite: this.buttonSpriteArray){
            if (sprite.getBoundingRectangle().contains(touchPos.x, Gdx.app.getGraphics().getHeight()-touchPos.y)){
                if (Gdx.input.isTouched()){
                    buttonClickSprite.setPosition(sprite.getX(), sprite.getY());
                    buttonClickSprite.draw(batch);
                }else {
                    buttonHoverSprite.setPosition(sprite.getX(), sprite.getY());
                    buttonHoverSprite.draw(batch);
                }
            }
        }

        batch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        splashSprite.getTexture().dispose();
    }
}