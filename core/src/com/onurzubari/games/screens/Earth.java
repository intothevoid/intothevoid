package com.onurzubari.games.screens;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.onurzubari.games.IntoTheVoid;

public class Earth implements Screen {
	
	//Global Variables.
	OrthographicCamera camera;
	
	float angularVelocity;
	Vector2 linearVelocity;
	
	private IntoTheVoid app;
	private Stage stage;
	FPSLogger logger;
	
	float width = IntoTheVoid.stageWidth;
	float height = IntoTheVoid.stageHeight;
	
	World space;
	Box2DDebugRenderer renderer;
	ContactListener collision;
	
	RayHandler rayHandler;
	
	Body player;
	
	Texture characterTexture;
	Sprite characterSprite;

	Texture starTexture;
	Texture groundTexture;
	Sprite groundSprite;
	
	SpriteBatch batchBackground;
	SpriteBatch batchForeground;

	ConeLight sunLight;
	
	Boolean upwards = false;
	String direction = "down";
	
	ShapeRenderer shapeRenderer = new ShapeRenderer();
	
	
	BitmapFont font;
	CharSequence controls = "Press 'solar' to decrease linear velocity.\n"
			+ "Press 'Q' to switch gameplay stage.\n"
			+ "Press 'M' to pause the music.\n"
			+ "Press 'P' to play the music.\n"
			+ "Press 'Esc' to exit.";
	
	CharSequence version = IntoTheVoid.version;
	
	public Earth(IntoTheVoid app){
		this.app = app;
		camera = app.middlegroundCamera;
		this.stage = new Stage(new StretchViewport(this.app.stageWidth, this.app.stageHeight, camera));
        
		//groundSprite = new Sprite(groundTexture,0,0,512,512);
		//groundSprite.setBounds(0, 0, app.stageWidth/2, 6f);
		
		characterTexture = new Texture(Gdx.files.internal("character.png"));
		//characterTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		characterSprite = new Sprite(characterTexture,0,0,characterTexture.getWidth(),characterTexture.getHeight());
		characterSprite.setBounds(0, 0, 1f,2.2f);
		
		
		starTexture = new Texture(Gdx.files.internal("hull.jpg"));
		starTexture.getUWrap();
		starTexture.getVWrap();
		starTexture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		TextureRegion nebulaTextureRegion = new TextureRegion(starTexture);
		nebulaTextureRegion.setRegion(0, 0, starTexture.getWidth()*20, starTexture.getHeight()*20);
		
		TextureRegionDrawable nebulaTextureRegionDrawable = new TextureRegionDrawable(nebulaTextureRegion);
        Image img = new Image();
        img.setDrawable(nebulaTextureRegionDrawable);
        img.setSize(IntoTheVoid.stageWidth,IntoTheVoid.stageHeight);
        stage.addActor(img);
        
        groundTexture = new Texture(Gdx.files.internal("ground.jpg"));
		groundTexture.getUWrap();
		groundTexture.getVWrap();
		groundTexture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		TextureRegion groundTextureRegion = new TextureRegion(groundTexture);
		groundTextureRegion.setRegion(0, 0, groundTexture.getHeight()*256, groundTexture.getHeight());
		TextureRegionDrawable groundTextureRegionDrawable = new TextureRegionDrawable(groundTextureRegion);
		Image groundImg = new Image();
        groundImg.setDrawable(groundTextureRegionDrawable);
        groundImg.setSize(IntoTheVoid.stageWidth, 6.0f);
        stage.addActor(groundImg);
		
		space = new World(new Vector2(0, -9.81f), false);
		renderer = new Box2DDebugRenderer();
		logger = new FPSLogger();
		batchForeground = new SpriteBatch();
		batchBackground = new SpriteBatch();
		font = new BitmapFont();
		
		
		//Body elements...
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(0, 50);
		
		player = space.createBody(playerDef);
		
		PolygonShape playerShape = new PolygonShape();
		playerShape.setAsBox(0.5f, 1.1f);
		
		FixtureDef playerFixture = new FixtureDef();
		playerFixture.shape = playerShape;
		playerFixture.density = 1f;
		playerFixture.restitution = 0;
		
		player.createFixture(playerFixture);
		player.setFixedRotation(true);
		
		BodyDef groundBodyDef = new BodyDef();
		groundBodyDef.position.set(IntoTheVoid.stageWidth /2, 3);
		Body groundBody = space.createBody(groundBodyDef);
		PolygonShape groundBox = new PolygonShape();
		groundBox.setAsBox(IntoTheVoid.stageWidth /2, 3.0f);
		groundBody.createFixture(groundBox, 0.0f);
		
		//Lights...
		rayHandler = new RayHandler(space);
	}

	@Override
	public void show() {
		System.out.println("Space screen!");
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
		camera.zoom = 0.5f;
		
		float lerp = 5f;
		Vector3 position = camera.position;
		position.x += (player.getPosition().x - position.x) * lerp * delta;
		position.y = (player.getPosition().y+5);
		
		if(Gdx.input.isKeyPressed(Keys.A) && player.getLinearVelocity().x >= -6) {
			player.setLinearVelocity(-6, player.getLinearVelocity().y);
			characterSprite.setFlip(true, false);
		}
 
		if(Gdx.input.isKeyPressed(Keys.D) && player.getLinearVelocity().x <= 6) {
			player.setLinearVelocity(6, player.getLinearVelocity().y);
			characterSprite.setFlip(false, false);
		}
		
		//Add a condition which the player is grounded.
		if(Gdx.input.isKeyPressed(Keys.W) && player.getLinearVelocity().y == 0) {
			player.setLinearVelocity(player.getLinearVelocity().x, 5);
		}
		
		if(!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.D) && player.getLinearVelocity().y == 0){
			player.setLinearVelocity(0, 0);
		}
 
		if(Gdx.input.isKeyPressed(Keys.S)) {
		}
		
		if(Gdx.input.isKeyPressed(Keys.SPACE)) {
			player.setAngularVelocity(0);
			player.setLinearVelocity(0, player.getLinearVelocity().y);
			player.setLinearVelocity(player.getLinearVelocity().x, 0);
		}
		
		if(Gdx.input.isKeyPressed(Keys.M)) {

		}
		
		if(Gdx.input.isKeyPressed(Keys.P)) {

		}
		
		if(Gdx.input.isKeyPressed(Keys.TAB)) {
			app.setScreen(app.centauri);
		}

		if(Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			Gdx.app.exit();
		}
		
		batchBackground.begin();
		batchBackground.setProjectionMatrix(camera.combined);		
		
		characterSprite.draw(batchBackground);
		characterSprite.setX(player.getPosition().x - (characterSprite.getWidth()/2));
		characterSprite.setY(player.getPosition().y - (characterSprite.getHeight()/2));
		characterSprite.setOriginCenter();
		
		/*
		groundSprite.draw(batchBackground);
		groundSprite.setX(0);
		groundSprite.setY(0);
		*/
		batchBackground.end();
		
		/*
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.setColor(130, 0, 0, 1);
		shapeRenderer.rect(0, 0, app.stageWidth, 6);
		shapeRenderer.end();
		*/
		
		rayHandler.setCombinedMatrix(camera);
		rayHandler.setAmbientLight(0.5f);
		rayHandler.updateAndRender();
		
		//Foreground
		batchForeground.begin();
		//batchForeground.draw(nebulaTexture, 0, 0);
		font.draw(batchForeground, controls, 20, height-20);
		font.draw(batchForeground, version, IntoTheVoid.stageWidth -(version.length()*8), IntoTheVoid.stageHeight -20);
		font.draw(batchForeground, player.getLinearVelocity().y+"", IntoTheVoid.stageWidth - 200, IntoTheVoid.stageHeight -50);
		batchForeground.end();
		
		logger.log();
		space.step(1/60f, 8	, 3);
		//renderer.render(solar, camera.combined);
		camera.update();
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
	}
}

