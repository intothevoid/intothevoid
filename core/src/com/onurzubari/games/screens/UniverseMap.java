package com.onurzubari.games.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.UI.PlanetFocus;
import com.onurzubari.games.objects.Star;

public class UniverseMap implements Screen, InputProcessor{
	
	private IntoTheVoid app;
	private Stage stage;
	World world;
	public String cameFrom;
    Vector3 touchPos;
    Box2DDebugRenderer renderer;
	
	Texture mapTexture;
	Sprite mapSprite;
	SpriteBatch batch;

	GlyphLayout layout;

	SpriteBatch batchForeground;
	BitmapFont font;
	CharSequence selected = "No star selected.";
	float fontX;
	float fontY;

	private PlanetFocus focus;
	
	Star sol, alphaCentauri, proximaCentauri, bernards, wise;
	
	public UniverseMap(final IntoTheVoid app) {
		this.app = app;

		world = new World(new Vector2(0, 0), false);
        renderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		batchForeground = new SpriteBatch();
		font = new BitmapFont();
		layout = new GlyphLayout(font, selected);
		fontX = (IntoTheVoid.windowWidth - layout.width) / 2;
		fontY = (IntoTheVoid.windowHeight + layout.height) / 10;

        touchPos = new Vector3();

		mapTexture = new Texture(Gdx.files.internal("starmap.png"));
		mapSprite = new Sprite(mapTexture, 0, 0, mapTexture.getWidth(), mapTexture.getHeight());
		mapSprite.setBounds(0, 0, IntoTheVoid.windowWidth, IntoTheVoid.windowHeight);
		
		sol = new Star(world, IntoTheVoid.windowWidth /2, IntoTheVoid.windowHeight /2, 32, "sol");
		alphaCentauri = new Star(world, IntoTheVoid.windowWidth /2 + IntoTheVoid.windowWidth * 0.05f, IntoTheVoid.windowHeight /2 + IntoTheVoid.windowWidth /20, 16, "alpha");
		proximaCentauri = new Star(world, alphaCentauri.body.getPosition().x + 20, alphaCentauri.body.getPosition().y - 5, 12, "proxima");
		bernards = new Star(world, IntoTheVoid.windowWidth /2 - IntoTheVoid.windowWidth * 0.07f, IntoTheVoid.windowHeight /2 + IntoTheVoid.windowWidth * 0.07f, 20, "bernards");
		wise = new Star(world, IntoTheVoid.windowWidth /2 - IntoTheVoid.windowWidth * 0.06f, IntoTheVoid.windowHeight /2 + IntoTheVoid.windowWidth * 0.12f, 20, "wise");

		sol.setTexture(Gdx.files.internal("star.png"));
		alphaCentauri.setTexture(Gdx.files.internal("star.png"));
		proximaCentauri.setTexture(Gdx.files.internal("star.png"));
		bernards.setTexture(Gdx.files.internal("star.png"));
		wise.setTexture(Gdx.files.internal("star.png"));

		focus = new PlanetFocus(batchForeground);
	}

	@Override
	public void show() {
        selected = "No star selected.";
        layout.setText(font, selected);
        fontX = (IntoTheVoid.windowWidth - layout.width) / 2;
        fontY = (IntoTheVoid.windowHeight + layout.height) / 10;
		this.stage = new Stage(new StretchViewport(IntoTheVoid.windowWidth, IntoTheVoid.windowHeight, app.middlegroundCamera));
		app.middlegroundCamera.zoom = 1;
	}

	private void textPlace(Star star){
        selected = star.name;
		if(cameFrom.equals("sol")){
			Gdx.input.setInputProcessor(app.solar);
			app.setScreen(app.solar);
			app.solar.destination = (String) selected;
			if(selected.equals("No star selected.")){
				app.solar.destination = null;
			}
		}
		if(cameFrom.equals("alpha")){
			Gdx.input.setInputProcessor(app.centauri);
			app.setScreen(app.centauri);
			app.centauri.destination = (String) selected;
			if(selected.equals("No star selected.")){
				app.centauri.destination = null;
			}
		}

		layout.setText(font, selected);
		fontX = (IntoTheVoid.windowWidth - layout.width) / 2;
		fontY = (IntoTheVoid.windowHeight + layout.height) / 10;
    }

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
		stage.act(delta);
		stage.draw();
		
		batch.begin();
		mapSprite.setX(0);
		mapSprite.setY(0);
		mapSprite.draw(batch);
		sol.update(batch);
		alphaCentauri.update(batch);
		proximaCentauri.update(batch);
		bernards.update(batch);
		wise.update(batch);
		batch.end();

        if (Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            if(sol.planetSprite.getBoundingRectangle().contains(touchPos.x, IntoTheVoid.windowHeight -touchPos.y)) {
                textPlace(sol);
            } else if(alphaCentauri.planetSprite.getBoundingRectangle().contains(touchPos.x, IntoTheVoid.windowHeight -touchPos.y)) {
                textPlace(alphaCentauri);
            } else if(proximaCentauri.planetSprite.getBoundingRectangle().contains(touchPos.x, IntoTheVoid.windowHeight -touchPos.y)){
                textPlace(proximaCentauri);
            } else if(bernards.planetSprite.getBoundingRectangle().contains(touchPos.x, IntoTheVoid.windowHeight -touchPos.y)){
                textPlace(bernards);
            } else if(wise.planetSprite.getBoundingRectangle().contains(touchPos.x, IntoTheVoid.windowHeight -touchPos.y)){
                textPlace(wise);
            } else {
                selected = "No star selected.";
            }
        }


		// Foreground
		batchForeground.begin();
		// batchForeground.draw(nebulaTexture, 0, 0);
		font.draw(batchForeground, layout, fontX, fontY);
		focus.draw(sol.body.getPosition().x, sol.body.getPosition().y, sol.radius/2);
		batchForeground.end();

        //renderer.render(world, app.camera.combined);
		world.step(1/60f, 8, 3);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.M) {
			if(cameFrom.equals("sol")){
				Gdx.input.setInputProcessor(app.solar);
				app.setScreen(app.solar);
                app.solar.destination = (String) selected;
                if(selected.equals("No star selected.")){
                    app.solar.destination = null;
                }
			}
            if(cameFrom.equals("alpha")){
                Gdx.input.setInputProcessor(app.centauri);
                app.setScreen(app.centauri);
                app.centauri.destination = (String) selected;
                if(selected.equals("No star selected.")){
                    app.centauri.destination = null;
                }
            }
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
