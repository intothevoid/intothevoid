package com.onurzubari.games.screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.TweenEngine.SpriteAccesor;

public class SplashScreen implements Screen {

	private Sprite splashSprite;
	private SpriteBatch batch;
	private TweenManager tweenManager;
	IntoTheVoid app;

	
	public SplashScreen(final IntoTheVoid app){
		this.app = app;
		Stage stage = new Stage(new StretchViewport(IntoTheVoid.stageWidth, IntoTheVoid.stageHeight, app.middlegroundCamera));
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void show() {
		batch = new SpriteBatch();
		tweenManager = new TweenManager();
		Tween.registerAccessor(Sprite.class, new SpriteAccesor());

		Texture splashTexture = new Texture("SplashScreen.jpg");
		splashTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		splashSprite = new Sprite(splashTexture);
		splashSprite.setSize(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight());

		Tween.set(splashSprite, SpriteAccesor.ALPHA).target(0).start(tweenManager);
		Tween.to(splashSprite, SpriteAccesor.ALPHA, 0.5f).target(1).start(tweenManager);
		Tween.to(splashSprite, SpriteAccesor.ALPHA, 0.5f).target(0).delay(1).setCallback(new TweenCallback() {
			@Override
			public void onEvent(int i, BaseTween<?> baseTween) {
				app.setScreen(app.mainMenu);
			}
		}).start(tweenManager);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		tweenManager.update(delta);

		batch.begin();
		splashSprite.draw(batch);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
	}
}
