package com.onurzubari.games.screens;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.bitfire.postprocessing.PostProcessor;
import com.bitfire.postprocessing.effects.Bloom;
import com.bitfire.postprocessing.effects.CrtMonitor;
import com.bitfire.postprocessing.effects.Zoomer;
import com.bitfire.postprocessing.filters.Combine;
import com.bitfire.postprocessing.filters.CrtScreen.Effect;
import com.bitfire.postprocessing.filters.CrtScreen.RgbMode;
import com.bitfire.postprocessing.filters.RadialBlur;
import com.bitfire.postprocessing.utils.FullscreenQuad;
import com.bitfire.utils.ShaderLoader;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.UI.PlanetFocus;
import com.onurzubari.games.UI.UI;
import com.onurzubari.games.objects.*;
import com.onurzubari.games.utils.*;

import java.util.ArrayList;
import java.util.Objects;

public class Solar implements Screen, InputProcessor {

    // Global Variables.
    private IntoTheVoid app;
    private Stage stage;
    private FPSLogger logger;
    
    private float time = 0;

    public Space space;
    private World world;
    private Box2DDebugRenderer renderer;

    private RayHandler rayHandler;

    private Player player;

    private Planet earth;
    private Moon moon;
    private int cometCount = 0;
    private Body comet[] = new Body[cometCount];

    private Sprite astSprite;

    private Sprite nebulaSprite;

    private DrawStarBackground background;

    private SpriteBatch batch;
    private SpriteBatch UIbatch;
    private SpriteBatch focusBatch;

    private ConeLight shipLight;
    private ConeLight sunLight;

    private Vector2 mousePos = new Vector2();

    private Enemy1 enemy1;

    private ShapeRenderer orbit = new ShapeRenderer();

    private BitmapFont font;
    private CharSequence controls = "Hold 'Space' to decrease linear velocity.\n"
            + "Press '1' to switch the ship light on/off.\n" + "Press '2' to switch the sun light on/off.\n"
            + "Press '3' for switching to ground prototype.\n" + "Press '4' to enable/disable debug renderer.\n"
            + "Press '4 for switching to debug stage.\n" + "Press 'Q' for switching to gameplay stage.\n"
            + "Press 'M' to open the universe map.";

    private CharSequence version = IntoTheVoid.version;

    private int zoomAmount = 1;

    private PostProcessor postProcessor;
    private CrtMonitor crt;
    private Zoomer zoomer;

    private JumpAnimation jumpAnimation;
    private boolean jumping = false;

    String destination = null;
    boolean jumped = false;

    private ControlHandler controlHandler;

    private UI ui;
    private PlanetFocus focus1;

    private ArrayList<Bullet> explodingList;
    private ArrayList<NPC> NPCList;

    private boolean debugLines = true;
    private boolean enemyShipSelected = false;

    private Matrix4 cameraMatrixCopy;
    Texture lightMap;
    TextureRegion fboRegion;
    FrameBuffer fbo;
    ShaderProgram lightShader;
    FullscreenQuad fullScreenQuad;

    Matrix4 defaultProjectionMatrix;

    private float rightFrame, leftFrame, upFrame, downFrame;

    private Double doubleCast;

    private Vector2 touchPos;

    @SuppressWarnings("static-access")
    public Solar(IntoTheVoid app) {
        this.app = app;
        this.stage = new Stage(new StretchViewport(app.stageWidth, app.stageHeight, app.foregroundCamera));
        space = new Space();
        world = space.world;

        batch = new SpriteBatch();
        UIbatch = new SpriteBatch();
        focusBatch = new SpriteBatch();
        explodingList = new ArrayList<>();
        NPCList = new ArrayList<>();
        defaultProjectionMatrix = UIbatch.getProjectionMatrix();
        touchPos = new Vector2();

        //****Player initialize****************	//
        player = new Player(app);
        player.setWorld(world);
        player.setWidth(0.5f);
        player.setHeight(0.5f);
        player.setPositionX(app.stageWidth / 2f + 25);
        player.setPositionY(app.stageHeight / 2f);
        player.setShip();
        player.createPlayerShip(Gdx.files.internal("enemy.png"), batch);
        space.debrisVector.add(player.ship.body);
        controlHandler = new ControlHandler(app, player);
        //*************************************	//

        createCollisionListener();

        enemy1 = new Enemy1(4,4.6315f, app.stageWidth /2, app.stageHeight /2 + 100, world);
        enemy1.createShip(Gdx.files.internal("y01.png"));
        space.debrisVector.add(enemy1.body);

        for(int i=0; i<1; i++){
            NPCList.add(new NPC("Patrol",0.7f,0.7f, app.stageWidth /2, app.stageHeight /2 + i*3, world, space, app));
        }

        earth = new Planet(space, app.stageWidth / 2, app.stageHeight / 2, 10);
        earth.setTexture(Gdx.files.internal("earth.png"));
        earth.body.getFixtureList().first().setSensor(true);
        moon = new Moon(space, earth, app.stageWidth / 2, app.stageHeight / 2 + earth.radius*2, earth.radius/4);
        moon.setTexture(Gdx.files.internal("moon.png"));
        moon.body.getFixtureList().first().setSensor(true);

        for (int i = 0; i < cometCount; i++) {
            float size = MathUtils.random(0.2f, 0.4f); //Hard coded for testing, it'll be changed.
            comet[i] = CreateBody.createBox(space, size, size, earth.body.getWorldCenter().x, earth.body.getWorldCenter().y - (earth.radius) - i * size*3);
            comet[i].setBullet(true);
            comet[i].setUserData(size);
        }

        Texture astTexture = new Texture(Gdx.files.internal("ast.png"));
        astSprite = new Sprite(astTexture, 0, 0, astTexture.getWidth(), astTexture.getHeight());
        astSprite.setBounds(0, 0, 1, 1);

        Texture nebulaTexture = new Texture(Gdx.files.internal("nebula.png"));
        nebulaTexture.setWrap(nebulaTexture.getUWrap().Repeat, nebulaTexture.getVWrap().Repeat);
        nebulaSprite = new Sprite(nebulaTexture, 0, 0, nebulaTexture.getWidth(), nebulaTexture.getHeight());
        nebulaSprite.setBounds(0, 0, nebulaTexture.getWidth()/10, nebulaTexture.getHeight()/10);
        nebulaSprite.setOrigin(0, 0);


        renderer = new Box2DDebugRenderer();
        logger = new FPSLogger();
        font = new BitmapFont();

        ui = new UI(UIbatch);
        focus1 = new PlanetFocus(focusBatch);

        // Body elements...
        float shipAngle = player.ship.body.getAngle() * MathUtils.radiansToDegrees;

        // Lights...
        rayHandler = new RayHandler(world);
        rayHandler.setAmbientLight(0.5f);

        for(NPC npc : this.NPCList){
            npc.attachLights(rayHandler);
        }

        sunLight = new ConeLight(rayHandler, 1000, Color.TAN, 200, 5, app.stageHeight - 8, 180, 90);
        sunLight.setSoftnessLength(20);
        sunLight.setPosition(player.ship.body.getPosition().x + app.foregroundCamera.viewportWidth*app.foregroundCamera.zoom, player.ship.body.getPosition().y + app.foregroundCamera.viewportHeight*app.foregroundCamera.zoom);

        /*float rotateSpeed = 1;
        float posX = app.stageWidth * MathUtils.cosDeg(rotateAngle);
        float posY = app.stageHeight * MathUtils.sinDeg(rotateAngle);
        this.planetCoord = new Vector2(parent.body.getPosition().x - body.getPosition().x,parent.body.getPosition().y - body.getPosition().y);
        Vector2 targetAngle = MathUtils.atan2(-planetCoord.x, planetCoord.y);
        this.body.setTransform(posX, posY, targetAngle);
        float rotateAngle = (rotateAngle+rotateSpeed) % 360;*/

        shipLight = new ConeLight(rayHandler, 200, Color.RED, IntoTheVoid.stageHeight, player.ship.body.getPosition().x +2, player.ship.body.getPosition().y, shipAngle, 30);
        shipLight.attachToBody(player.ship.body);
        shipLight.setIgnoreAttachedBody(true);

        app.middlegroundCamera.position.x = player.ship.body.getPosition().x;
        app.middlegroundCamera.position.y = player.ship.body.getPosition().y;

        app.foregroundCamera.position.x = player.ship.body.getPosition().x;
        app.foregroundCamera.position.y = player.ship.body.getPosition().y;

        ShaderLoader.BasePath = "shaders/";
        postProcessor = new PostProcessor( false, false, true );
        Bloom bloom = new Bloom((int) (Gdx.graphics.getWidth() * 0.25f), (int) (Gdx.graphics.getHeight() * 0.25f));
        bloom.enableBlending(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_COLOR);
        bloom.setBaseIntesity(1f);
        bloom.setBaseSaturation(1f);
        bloom.setBloomIntesity(1f);
        bloom.setBloomSaturation(1f);
        bloom.setBlurAmount(1f);

        int effects = Effect.TweakContrast.v | Effect.PhosphorVibrance.v | Effect.Scanlines.v | Effect.Tint.v;
        crt = new CrtMonitor(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, false, RgbMode.ChromaticAberrations, effects );
        Combine combine = crt.getCombinePass();
        combine.setSource1Intensity( 0f );
        combine.setSource2Intensity( 1f );
        combine.setSource1Saturation( 0f );
        combine.setSource2Saturation( 1f );

        zoomer = new Zoomer(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), RadialBlur.Quality.VeryHigh);
        zoomer.setBlurStrength(0);
        postProcessor.addEffect(zoomer);

        jumpAnimation = new JumpAnimation();

        cameraMatrixCopy = new Matrix4();

        fbo = new FrameBuffer(Pixmap.Format.RGBA8888, 1920, 1080, true);
        fboRegion = new TextureRegion(fbo.getColorBufferTexture());
        lightShader = new ShaderProgram(Gdx.files.internal("shaders/layervertex.vsh"), Gdx.files.internal("shaders/layerfrag.fsh"));
        fullScreenQuad = new FullscreenQuad();

        this.stage = new Stage(new StretchViewport(IntoTheVoid.stageWidth, IntoTheVoid.stageHeight, app.foregroundCamera));
        background = new DrawStarBackground(this.stage, IntoTheVoid.stageWidth * app.maxZoom, IntoTheVoid.stageHeight * app.maxZoom);
    }

    @Override
    public void show() {
        System.out.println("Solar screen!");
        if(jumped){
            zoomer.setBlurStrength(1.2f);
        }
        background.setBackgroundTextTexture("sol.png");

        app.middlegroundCamera.position.x = player.ship.body.getPosition().x;
        app.middlegroundCamera.position.y = player.ship.body.getPosition().y;

        app.foregroundCamera.position.x = player.ship.body.getPosition().x;
        app.foregroundCamera.position.y = player.ship.body.getPosition().y;
    }

    private Vector3 tmpv3 = new Vector3();

    private void unproject(Camera cam, Vector2 vec) {
        tmpv3.set(vec.x, vec.y, 0f);
        cam.unproject(tmpv3);
        vec.set(tmpv3.x, tmpv3.y);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
        postProcessor.capture();
        //crt.setTime( time );
        stage.act(delta);
        controlHandler.handle(delta);

        app.backgroundCamera.update();
        app.middlegroundCamera.update();
        app.foregroundCamera.update();

        moon.rotateMoon();
        if(jumped){
            jumpAnimation.jumpingContinue(zoomer, delta, true);
            jumping = jumpAnimation.updateStatus();
            if(!jumping){
                jumped = false;
            }
        }

        //
        cameraMatrixCopy.set(app.foregroundCamera.combined);
        rayHandler.setCombinedMatrix(cameraMatrixCopy.scale(1.0f, 1.0f, 1.0f), app.foregroundCamera.position.x,
                app.foregroundCamera.position.y, app.foregroundCamera.viewportWidth * app.foregroundCamera.zoom * 1.0f,
                app.foregroundCamera.viewportHeight * app.foregroundCamera.zoom * 1.0f);
        rayHandler.update();
        rayHandler.render();
        lightMap = rayHandler.getLightMapTexture();

        touchPos.set(Gdx.input.getX(), Gdx.input.getY());
        unproject(app.foregroundCamera, touchPos);

        fbo.begin();
        {
            Gdx.gl.glClearColor(0, 0, 0, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            batch.begin();
            rightFrame = player.ship.body.getPosition().x + (app.foregroundCamera.viewportWidth/2*app.foregroundCamera.zoom);
            leftFrame = player.ship.body.getPosition().x - (app.foregroundCamera.viewportWidth/2*app.foregroundCamera.zoom);
            upFrame = player.ship.body.getPosition().y + (app.foregroundCamera.viewportHeight/2*app.foregroundCamera.zoom);
            downFrame = player.ship.body.getPosition().y - (app.foregroundCamera.viewportHeight/2*app.foregroundCamera.zoom);
            background.repeatBackground(rightFrame, leftFrame, upFrame, downFrame);
            batch.end();

            batch.enableBlending();
            batch.setProjectionMatrix(app.foregroundCamera.combined);
            batch.begin();

            batch.end();
            batch.setProjectionMatrix(app.foregroundCamera.combined);
            batch.begin();

            //*******************************
            nebulaSprite.setX((IntoTheVoid.stageWidth / 2 - nebulaSprite.getWidth() / 2) - (player.ship.body.getPosition().x / 10));
            nebulaSprite.setY((IntoTheVoid.stageHeight / 2 - nebulaSprite.getWidth() / 2) - player.ship.body.getPosition().y / 10);
            nebulaSprite.draw(batch);

            earth.update(batch);
            moon.update(batch);
            enemy1.update(batch);
            batch.end();

            focusBatch.setProjectionMatrix(app.foregroundCamera.combined);
            focusBatch.begin();
            if (enemyShipSelected){
                if(!ui.npc.destroyed){
                    focus1.draw(ui.npc.spriteNPC.getX() + ui.npc.spriteNPC.getWidth()/2, ui.npc.spriteNPC.getY() + ui.npc.spriteNPC.getHeight()/2, ui.npc.spriteNPC.getWidth());
                }
            }
            focusBatch.end();

            batch.begin();
            for(NPC npc : this.NPCList){
                if (npc.alive){
                    npc.update(batch, player.getShip().body, delta);
                } else {
                    if (!npc.destroyed){
                        npc.destroyNPC();
                        npc.body.destroyFixture(npc.body.getFixtureList().first());
                        this.world.destroyBody(npc.body);
                        this.player.points += 100;
                        if (player.nextLevelExp < (player.exp + npc.expValue)){
                            player.levelUp(npc.expValue);
                        } else {
                            this.player.exp += npc.expValue;
                            npc.spriteNPC.getTexture().dispose();
                        }
                    }
                }
            }

            player.update(batch, delta);

            //****** Engine effect *********/
            controlHandler.setRunning(false);
            //******************************/

            for (int i = 0; i < cometCount; i++) {
                astSprite.draw(batch);
                astSprite.setBounds(0,0, (float)comet[i].getUserData()*2, (float)comet[i].getUserData()*2);
                astSprite.setX(comet[i].getPosition().x - (astSprite.getWidth() / 2));
                astSprite.setY(comet[i].getPosition().y - (astSprite.getHeight() / 2));
                astSprite.setOriginCenter();
                astSprite.setRotation(MathUtils.radiansToDegrees*comet[i].getAngle());
            }

            /*if (!this.explodingList.isEmpty()){

            for(Bullet bullet : this.explodingList){              //ConcurrentModificationException
                if (bullet.explode(delta, batch)){
                    explodingList.remove(bullet);
                }
            }


                //explodingList.removeIf(bullet -> !bullet.explode(delta, batch));
            }*/

            player.removeBullet(delta);
            if(app.bullets != null && app.bullets.size()>0){
                for(Bullet bullet : app.bullets){
                    if (bullet.exploding){
                        bullet.updateExplosion();
                    } else {
                        bullet.update(batch);
                    }
                }
            }
            //*****************************************************
            batch.end();
        }
        fbo.end();

        batch.setProjectionMatrix(app.foregroundCamera.combined);
        batch.disableBlending();

        stage.draw();
        focusBatch.setProjectionMatrix(app.foregroundCamera.combined);
        focusBatch.begin();
        focus1.draw(moon.posX, moon.posY, moon.radius);
        background.drawBackgroundText(focusBatch);
        if (enemyShipSelected){
            if(!ui.npc.destroyed){
                focus1.draw(ui.npc.spriteNPC.getX() + ui.npc.spriteNPC.getWidth()/2, ui.npc.spriteNPC.getY() + ui.npc.spriteNPC.getHeight()/2, ui.npc.spriteNPC.getWidth());
            }
        }
        for(NPC npc : this.NPCList){
            if (npc.alive){
                float hull = npc.currentHull/npc.hull;
                float shield = npc.currentShield/npc.shield;
                ui.drawEnemySmallIndicator(npc.spriteNPC, focusBatch, hull, shield);
            }
        }


        ui.drawEnemySmallIndicator(enemy1.enemySprite, focusBatch, enemy1.currentHull/enemy1.hull, enemy1.currentShield /enemy1.shield);
        focusBatch.end();


        Gdx.gl20.glActiveTexture(GL20.GL_TEXTURE0);
        fboRegion.getTexture().bind();

        Gdx.gl20.glActiveTexture(GL20.GL_TEXTURE1);
        lightMap.bind();

        Gdx.gl20.glEnable(Gdx.gl20.GL_BLEND);
        Gdx.gl20.glBlendFunc(Gdx.gl20.GL_SRC_ALPHA, Gdx.gl20.GL_ONE_MINUS_SRC_ALPHA);

        lightShader.begin();
        lightShader.setUniformf("ambient_color", sunLight.getColor());
        lightShader.setUniformi("u_texture0", 0);
        lightShader.setUniformi("u_texture1", 1);
        fullScreenQuad.render(lightShader);
        lightShader.end();

        Gdx.gl20.glDisable(Gdx.gl20.GL_BLEND);

        Gdx.gl20.glActiveTexture(GL20.GL_TEXTURE0);

        UIbatch.begin();
        //batchForeground.draw(nebulaTexture, 0, 0);
        //font.draw(UIbatch, controls, 20, IntoTheVoid.windowHeight - 20);
        //font.draw(UIbatch, version, IntoTheVoid.windowWidth - (version.length() * 8), IntoTheVoid.windowHeight - 20);
        time = time + delta;
        //font.draw(UIbatch, "Session = " + Math.round(time) + "sec", IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 50);
        //font.draw(UIbatch, Math.round(player.ship.body.getPosition().x) + " , " + Math.round(player.ship.body.getPosition().y), IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 80);
        //font.draw(UIbatch, "Fuel: "+player.ship.currentFuel+"/"+player.ship.fuelCapacity, IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 110);
        font.draw(UIbatch, "Touch Position: "+ touchPos, IntoTheVoid.windowWidth - 400, IntoTheVoid.windowHeight - 140);
        ui.drawUI(player);
        if (enemyShipSelected){
            ui.drawEnemyUI();
        }
        UIbatch.end();
        //

        if(jumping && !jumped){
            if (destination == null){
                System.out.println("Select a star to jump!");
                jumping = false;
            }else if(destination.equals("sol")){
                System.out.println("You are already in Sol!");
                jumping = false;
            }else{
                jumpAnimation.update(zoomer, delta, true);
                jumping = jumpAnimation.updateStatus();
                if(!jumping){
                    switch (destination){
                        case "alpha":
                            Gdx.input.setInputProcessor(app.centauri);
                            app.setScreen(app.centauri);
                            app.centauri.jumped = true;
                            break;
                        case "proxima":
                            break;
                        case "bernards":
                            break;
                        case "wise":
                            break;
                    }
                }
            }
        }


        float lerp = 5f;
        Vector3 position = app.foregroundCamera.position;
        position.x += (player.ship.body.getPosition().x - position.x) * lerp * delta;
        position.y += (player.ship.body.getPosition().y - position.y) * lerp * delta;

        if(app.foregroundCamera.zoom < zoomAmount){
            app.foregroundCamera.zoom += (zoomAmount-app.foregroundCamera.zoom) * lerp/2 * delta;
        }
        else if(app.foregroundCamera.zoom > zoomAmount){
            app.foregroundCamera.zoom += (zoomAmount-app.foregroundCamera.zoom) * lerp/2 * delta;
        }
        if(app.foregroundCamera.zoom > zoomAmount-0.001 && app.foregroundCamera.zoom < zoomAmount+0.001 && app.foregroundCamera.zoom != zoomAmount){
            app.foregroundCamera.zoom = Math.round(app.foregroundCamera.zoom);
        }

        if (debugLines){

            orbit.begin(ShapeRenderer.ShapeType.Line);
            orbit.setProjectionMatrix(app.foregroundCamera.combined);
            orbit.line(mousePos, player.ship.body.getPosition());
            orbit.circle(earth.body.getPosition().x, earth.body.getPosition().y,earth.body.getFixtureList().first().getShape().getRadius() * 2);

            for(NPC npc: this.NPCList){
                if (Objects.equals(npc.status, "chasing")){
                    orbit.line(player.ship.body.getPosition(), npc.body.getPosition());
                } else if (Objects.equals(npc.status, "close")){
                    orbit.setColor(0, 255, 255, 1);
                    orbit.line(player.ship.body.getPosition(), npc.body.getPosition());
                    orbit.setColor(255, 0, 255, 1);
                }
            }
            orbit.end();

            mousePos.set(Gdx.input.getX(), Gdx.input.getY());
            unproject(app.foregroundCamera, mousePos);
            float dx = mousePos.x - player.ship.body.getPosition().x;
            float dy = mousePos.y - player.ship.body.getPosition().y;
            float angle = (float)Math.atan2(dy, dx);
            player.ship.body.setTransform(player.ship.body.getPosition().x, player.ship.body.getPosition().y, angle);
        }



        sunLight.setPosition(player.ship.body.getPosition().x + app.foregroundCamera.viewportWidth*app.foregroundCamera.zoom, player.ship.body.getPosition().y + app.foregroundCamera.viewportHeight*app.foregroundCamera.zoom);
        sunLight.setDistance(80*app.foregroundCamera.zoom);

        RadialPlanetGravity.apply(space.planetVector, space.debrisVector);

        //logger.log();
        renderer.render(world, app.foregroundCamera.combined);
        //Gdx.app.log("step", "step");
        world.step(1/60f, 12, 6);
    }

    @Override
    public void hide() {

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        postProcessor.rebind();
    }

    @Override
    public void dispose() {
        postProcessor.dispose();
    }

    // Input handlers.
    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Keys.NUM_1) {
            if (shipLight.isActive()) {
                shipLight.setActive(false);
            } else {
                shipLight.setActive(true);
            }
        }

        if (keycode == Keys.NUM_2) {
            if (sunLight.isActive()) {
                sunLight.setActive(false);
            } else {
                sunLight.setActive(true);
            }
        }

        if (keycode == Keys.NUM_3) {
            app.setScreen(app.earth);
        }

        if (keycode == Keys.NUM_4) {
            if (renderer.isDrawBodies()) {
                renderer.setDrawBodies(false);
                renderer.setDrawAABBs(false);
                renderer.setDrawContacts(false);
                renderer.setDrawInactiveBodies(false);
                renderer.setDrawJoints(false);
                renderer.setDrawVelocities(false);
                postProcessor.setEnabled(false);
                debugLines = false;

            } else {
                renderer.setDrawBodies(true);
                renderer.setDrawAABBs(false);
                renderer.setDrawContacts(true);
                renderer.setDrawInactiveBodies(true);
                renderer.setDrawJoints(true);
                postProcessor.setEnabled(true);
                renderer.setDrawVelocities(true);
                debugLines = true;
            }
        }

        if (keycode == Keys.NUM_5) {
            jumping = true;
        }

        if (keycode == Keys.NUM_6) {
            for(int i=0; i<5; i++){
                NPCList.add(new NPC("Patrol",0.5f,0.5f, IntoTheVoid.stageWidth /2, IntoTheVoid.stageHeight /2 + i*3, world, space, app));
            }
        }

        if (keycode == Keys.M) {
            app.map.cameFrom = "sol";
            Gdx.input.setInputProcessor(app.map);
            app.setScreen(app.map);
        }

        if (keycode == Keys.ESCAPE) {
            app.setScreen(app.mainMenu);
            //Gdx.input.setInputProcessor(null);
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            for(NPC npc : this.NPCList) {
                if (npc.spriteNPC.getBoundingRectangle().contains(touchPos.x, touchPos.y)) {
                    enemyShipSelected = true;
                    ui.setSelectedNPC(npc);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if(zoomAmount >= app.minZoom && zoomAmount <= app.maxZoom){
            zoomAmount += amount;
            if(zoomAmount < app.minZoom){
                zoomAmount = app.minZoom;
            }
            else if(zoomAmount > app.maxZoom){
                zoomAmount = app.maxZoom;
            }
        }
        return false;
    }

    private void createCollisionListener() {
        world.setContactListener(new ContactListener() {

            @Override
            public void beginContact(Contact contact) {

            }

            @Override
            public void endContact(Contact contact) {
                Fixture fixtureA = contact.getFixtureA();
                Fixture fixtureB = contact.getFixtureB();
                //Gdx.app.log("beginContact", "between " + fixtureA.getBody().getPosition().toString() + " and " + fixtureB.getBody().getPosition().toString());

                Body b1 = fixtureA.getBody();
                Body b2 = fixtureB.getBody();

                Object o1 = b1.getUserData();
                Object o2 = b2.getUserData();

                //Gdx.app.log("Object1 ", "Object's class: " + o1.toString());
                //Gdx.app.log("Object2 ", "Object's class: " + o2.toString());

                if(o1.getClass() == Bullet.class){
                    if(!fixtureB.isSensor()){
                        ((Bullet) o1).explode();
                        app.getSoundManager().play(SoundHandler.ITVSound.Explosion2);
                        /*Bullet bullet1 = (Bullet)o1;
                        Gdx.app.log("Bullet1", "index: " + bullet1.index);
                        Gdx.app.log("Size", "" + player.bullets.size());
                        if (player.bullets.size() > bullet1.index) {
                            player.bullets.get(bullet1.index).lifeTimer = 0;
                            app.explosion.play(0.05f, MathUtils.random(1f,2f), 1);
                            explodingList.add(player.bullets.get(bullet1.index));
                            explosionAnimation.setExplosionCoordinates(explodingList, player.bullets.get(bullet1.index).body.getPosition().x, player.bullets.get(bullet1.index).body.getPosition().y);
                        }*/
                    }
                }
                System.out.println();

                if(o2.getClass() == Bullet.class) {
                    if(!fixtureA.isSensor()) {
                        if (o1.getClass() == NPC.class && !((Bullet) o2).exploding){
                            //((NPC)o1).hostile = true;
                            ((NPC)o1).takeDamage(((Bullet) o2).damage);
                            app.getSoundManager().play(SoundHandler.ITVSound.Explosion2);
                        }

                        else if (o1.getClass() == Player.class && !((Bullet) o2).exploding){
                            ((Player)o1).ship.takeDamage(((Bullet) o2).damage);
                            app.getSoundManager().play(SoundHandler.ITVSound.Explosion2);
                        } else if (o1.getClass() == Enemy1.class && !((Bullet) o2).exploding){
                            ((Enemy1)o1).takeDamage(((Bullet) o2).damage);
                            app.getSoundManager().play(SoundHandler.ITVSound.Explosion2);
                        }
                        ((Bullet) o2).explode();

                        /*Bullet bullet2 = (Bullet) o2;
                        Gdx.app.log("Bullet2", "index: " + bullet2.index);
                        Gdx.app.log("Size", "" + player.bullets.size());

                        if (player.bullets.size() > bullet2.index) {
                            player.bullets.get(bullet2.index).lifeTimer = 0;
                            app.explosion2.play(0.05f, MathUtils.random(1f, 2f), 1);
                            explodingList.add(player.bullets.get(bullet2.index));
                            explosionAnimation.setExplosionCoordinates(explodingList, player.bullets.get(bullet2.index).body.getPosition().x, player.bullets.get(bullet2.index).body.getPosition().y);
                        }*/
                    }
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {
            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {
            }

        });
    }
}