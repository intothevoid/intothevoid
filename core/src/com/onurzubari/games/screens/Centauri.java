package com.onurzubari.games.screens;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.bitfire.postprocessing.PostProcessor;
import com.bitfire.postprocessing.effects.Bloom;
import com.bitfire.postprocessing.effects.CrtMonitor;
import com.bitfire.postprocessing.effects.Zoomer;
import com.bitfire.postprocessing.filters.Combine;
import com.bitfire.postprocessing.filters.CrtScreen.Effect;
import com.bitfire.postprocessing.filters.CrtScreen.RgbMode;
import com.bitfire.postprocessing.filters.RadialBlur;
import com.bitfire.utils.ShaderLoader;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.UI.PlanetFocus;
import com.onurzubari.games.UI.UI;
import com.onurzubari.games.objects.*;
import com.onurzubari.games.utils.*;

import java.util.ArrayList;

public class Centauri implements Screen, InputProcessor {

	// Global Variables.
	private IntoTheVoid app;
	private Stage stage;
	private FPSLogger logger;

	private float time = 0;

	private Space space;
	private World world;
	private Box2DDebugRenderer renderer;

	private RayHandler rayHandler;

	private Player player;

	private Planet earth;
	private Planet valhalla;
	private Moon moon;
	private int cometCount = 0;
	private Body comet[] = new Body[cometCount];

	private Sprite astSprite;

	private Sprite nebulaSprite;

	private DrawStarBackground background;

	private SpriteBatch batchBackground;
	private SpriteBatch batchForeground;

	private ConeLight shipLight;
	private ConeLight sunLight;

	private Vector2 mousePos = new Vector2();

	private Enemy1 enemy1;

	private ShapeRenderer orbit = new ShapeRenderer();

	private BitmapFont font;
	private CharSequence controls = "Hold 'solar' to decrease linear velocity.\n"
			+ "Press '1' to switch the ship light on/off.\n" + "Press '2' to switch the sun light on/off.\n"
			+ "Press '3' for switching to ground prototype.\n" + "Press '4' to enable/disable debug renderer.\n"
			+ "Press 'TAB for switching to debug stage.\n" + "Press 'Q' for switching to gameplay stage.\n"
			+ "Press 'M' to toggle the music on and off.\n" + "Press 'Esc' to exit.";

	private CharSequence version = IntoTheVoid.version;

	private int zoomAmount = 1;

	private PostProcessor postProcessor;
	private CrtMonitor crt;
	private Zoomer zoomer;

	private JumpAnimation jumpAnimation;
	private boolean jumping = false;

	String destination = null;
	boolean jumped = false;

	private ControlHandler controlHandler;

	private UI ui;
	private PlanetFocus focus1;
	private PlanetFocus focus2;

	private ArrayList<Bullet> explodingList;

	private boolean debugLines = true;

	@SuppressWarnings("static-access")
	public Centauri(IntoTheVoid app) {
		this.app = app;
		this.stage = new Stage(new StretchViewport(app.stageWidth, app.stageHeight, app.middlegroundCamera));
		space = new Space();
		world = space.world;

		batchForeground = new SpriteBatch();
		batchBackground = new SpriteBatch();

		explodingList = new ArrayList<>();

		//****Player initialize****************	//
		player = new Player(app);
		player.setWorld(world);
		player.setWidth(0.5f);
		player.setHeight(0.5f);
		player.setPositionX(app.stageWidth / 2f + 25);
		player.setPositionY(app.stageHeight / 2f);
		player.setShip();
		player.createPlayerShip(Gdx.files.internal("enemy.png"), batchBackground);
		space.debrisVector.add(player.ship.body);
		controlHandler = new ControlHandler(app, player);
		//*************************************	//

		createCollisionListener();

		enemy1 = new Enemy1(4,5.176f, app.stageWidth /2, app.stageHeight /2 + 100, world);
		enemy1.createShip(Gdx.files.internal("y01.png"));
		space.debrisVector.add(enemy1.body);

		earth = new Planet(space, app.stageWidth / 2, app.stageHeight / 2, 10);
		earth.setTexture(Gdx.files.internal("alpha1.png"));
		//valhalla = new Planet(space, app.stageWidth / 2 - earth.radius * 8, app.stageHeight / 2, 14);
		//valhalla.setTexture(Gdx.files.internal("alpha3.png"));
		moon = new Moon(space, earth, app.stageWidth / 2, app.stageHeight / 2 + earth.radius*2, earth.radius/4);
		moon.setTexture(Gdx.files.internal("alpha2.png"));

		for (int i = 0; i < cometCount; i++) {
			float size = MathUtils.random(0.2f, 0.4f); //Hard coded for testing, it'll be changed.
			comet[i] = CreateBody.createBox(space, size, size, earth.body.getWorldCenter().x, earth.body.getWorldCenter().y - (earth.radius) - i * size*3);
			comet[i].setBullet(true);
			comet[i].setUserData(size);
		}

		Texture astTexture = new Texture(Gdx.files.internal("ast.png"));
		astSprite = new Sprite(astTexture, 0, 0, astTexture.getWidth(), astTexture.getHeight());
		astSprite.setBounds(0, 0, 1, 1);

		Texture nebulaTexture = new Texture(Gdx.files.internal("nebula.png"));
		nebulaTexture.setWrap(nebulaTexture.getUWrap().Repeat, nebulaTexture.getVWrap().Repeat);
		nebulaSprite = new Sprite(nebulaTexture, 0, 0, nebulaTexture.getWidth(), nebulaTexture.getHeight());
		nebulaSprite.setBounds(0, 0, nebulaTexture.getWidth()/10, nebulaTexture.getHeight()/10);
		nebulaSprite.setOrigin(0, 0);


		renderer = new Box2DDebugRenderer();
		logger = new FPSLogger();
		font = new BitmapFont();

		ui = new UI(batchForeground);
		focus1 = new PlanetFocus(batchBackground);
		focus2 = new PlanetFocus(batchBackground);

		// Body elements...
		float shipAngle = player.ship.body.getAngle() * MathUtils.radiansToDegrees;

		// Lights...
		rayHandler = new RayHandler(world);
		rayHandler.setAmbientLight(0.5f);

		sunLight = new ConeLight(rayHandler, 1000, Color.FIREBRICK, 200, 5, app.stageHeight - 8, 0, 180);
		sunLight.setSoftnessLength(20);

        /*float rotateSpeed = 1;
        float posX = app.stageWidth * MathUtils.cosDeg(rotateAngle);
        float posY = app.stageHeight * MathUtils.sinDeg(rotateAngle);
        this.planetCoord = new Vector2(parent.body.getPosition().x - body.getPosition().x,parent.body.getPosition().y - body.getPosition().y);
        Vector2 targetAngle = MathUtils.atan2(-planetCoord.x, planetCoord.y);
        this.body.setTransform(posX, posY, targetAngle);
        float rotateAngle = (rotateAngle+rotateSpeed) % 360;*/

		shipLight = new ConeLight(rayHandler, 200, Color.GRAY, IntoTheVoid.stageHeight, player.ship.body.getPosition().x +2, player.ship.body.getPosition().y, shipAngle, 30);
		shipLight.attachToBody(player.ship.body);
		shipLight.setIgnoreAttachedBody(true);

		app.middlegroundCamera.position.x = player.ship.body.getPosition().x;
		app.middlegroundCamera.position.y = player.ship.body.getPosition().y;

		ShaderLoader.BasePath = "shaders/";
		postProcessor = new PostProcessor( false, false, true );
		Bloom bloom = new Bloom((int) (Gdx.graphics.getWidth() * 0.25f), (int) (Gdx.graphics.getHeight() * 0.25f));
		bloom.enableBlending(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_COLOR);
		bloom.setBaseIntesity(1f);
		bloom.setBaseSaturation(1f);
		bloom.setBloomIntesity(1f);
		bloom.setBloomSaturation(1f);
		bloom.setBlurAmount(1f);

		int effects = Effect.TweakContrast.v | Effect.PhosphorVibrance.v | Effect.Scanlines.v | Effect.Tint.v;
		crt = new CrtMonitor(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, false, RgbMode.ChromaticAberrations, effects );
		Combine combine = crt.getCombinePass();
		combine.setSource1Intensity( 0f );
		combine.setSource2Intensity( 1f );
		combine.setSource1Saturation( 0f );
		combine.setSource2Saturation( 1f );

		zoomer = new Zoomer(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), RadialBlur.Quality.VeryHigh);
		zoomer.setBlurStrength(0);
		postProcessor.addEffect(zoomer);

		jumpAnimation = new JumpAnimation();
	}

	@Override
	public void show() {
		System.out.println("Alpha screen!");
		this.stage = new Stage(new StretchViewport(IntoTheVoid.stageWidth, IntoTheVoid.stageHeight, app.middlegroundCamera));
		if(jumped){
			zoomer.setBlurStrength(1.2f);
		}
		background = new DrawStarBackground(this.stage, IntoTheVoid.stageWidth *app.maxZoom, IntoTheVoid.stageHeight *app.maxZoom);
		background.setBackgroundTextTexture("alpha.png");
	}

	private Vector3 tmpv3 = new Vector3();

	private void unproject(Camera cam, Vector2 vec) {
		tmpv3.set(vec.x, vec.y, 0f);
		cam.unproject(tmpv3);
		vec.set(tmpv3.x, tmpv3.y);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
		postProcessor.capture();
		crt.setTime( time );
		stage.act(delta);
		stage.draw();
		controlHandler.handle(delta);
		app.middlegroundCamera.update();
		moon.rotateMoon();
		if(jumped){
			jumpAnimation.jumpingContinue(zoomer, delta, true);
			jumping = jumpAnimation.updateStatus();
			if(!jumping){
				jumped = false;
			}
		}

		if(jumping && !jumped){
			if (destination == null){
				System.out.println("Select a star to jump!");
				jumping = false;
			}else if(destination.equals("alpha")){
				System.out.println("You are already in Alpha Centauri!");
				jumping = false;
			}else{
				jumpAnimation.update(zoomer, delta, true);
				jumping = jumpAnimation.updateStatus();
				if(!jumping){
					switch (destination){
						case "sol":
							Gdx.input.setInputProcessor(app.solar);
							app.setScreen(app.solar);
							app.solar.jumped = true;
							break;
						case "proxima":
							break;
						case "bernards":
							break;
						case "wise":
							break;
					}
				}
			}
		}

		background.repeatBackground( player.ship.body.getPosition().x + (app.middlegroundCamera.viewportWidth/2*app.middlegroundCamera.zoom), player.ship.body.getPosition().x - (app.middlegroundCamera.viewportWidth/2*app.middlegroundCamera.zoom), player.ship.body.getPosition().y + (app.middlegroundCamera.viewportHeight/2*app.middlegroundCamera.zoom), player.ship.body.getPosition().y - (app.middlegroundCamera.viewportHeight/2*app.middlegroundCamera.zoom));

		float lerp = 5f;
		Vector3 position = app.middlegroundCamera.position;
		position.x += (player.ship.body.getPosition().x - position.x) * lerp * delta;
		position.y += (player.ship.body.getPosition().y - position.y) * lerp * delta;

		if(app.middlegroundCamera.zoom < zoomAmount){
			app.middlegroundCamera.zoom += (zoomAmount-app.middlegroundCamera.zoom) * lerp/2 * delta;
		}
		else if(app.middlegroundCamera.zoom > zoomAmount){
			app.middlegroundCamera.zoom += (zoomAmount-app.middlegroundCamera.zoom) * lerp/2 * delta;
		}
		if(app.middlegroundCamera.zoom > zoomAmount-0.001 && app.middlegroundCamera.zoom < zoomAmount+0.001 && app.middlegroundCamera.zoom != zoomAmount){
			app.middlegroundCamera.zoom = Math.round(app.middlegroundCamera.zoom);
		}

		// Key polling.


		orbit.begin(ShapeType.Line);

		if (debugLines){
			orbit.line(mousePos, player.ship.body.getPosition());
			orbit.circle(earth.body.getPosition().x, earth.body.getPosition().y,earth.body.getFixtureList().first().getShape().getRadius() * 2);
			mousePos.set(Gdx.input.getX(), Gdx.input.getY());
			unproject(app.middlegroundCamera, mousePos);
			float dx = mousePos.x - player.ship.body.getPosition().x;
			float dy = mousePos.y - player.ship.body.getPosition().y;
			float angle = (float)Math.atan2(dy, dx);
			player.ship.body.setTransform(player.ship.body.getPosition().x, player.ship.body.getPosition().y, angle);

			batchBackground.begin();
			focus1.draw(moon.posX, moon.posY, moon.radius);
			focus2.draw(earth.body.getPosition().x, earth.body.getPosition().y, earth.radius);
			background.drawBackgroundText(batchBackground);
			batchBackground.end();

			// Foreground
			batchForeground.begin();
			//batchForeground.draw(nebulaTexture, 0, 0);
			font.draw(batchForeground, controls, 20, IntoTheVoid.windowHeight - 20);
			font.draw(batchForeground, version, IntoTheVoid.windowWidth - (version.length() * 8), IntoTheVoid.windowHeight - 20);
			time = time + delta;
			font.draw(batchForeground, "Session = " + Math.round(time) + "sec", IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 50);
			font.draw(batchForeground, Math.round(player.ship.body.getPosition().x) + " , " + Math.round(player.ship.body.getPosition().y), IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 80);
			font.draw(batchForeground, "Fuel: "+player.ship.currentFuel+"/"+player.ship.fuelCapacity, IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 110);
			font.draw(batchForeground, "Exploding arrayList size: "+this.explodingList.isEmpty(), IntoTheVoid.windowWidth - 200, IntoTheVoid.windowHeight - 140);
			ui.drawUI(player);
			batchForeground.end();
		}


		orbit.setProjectionMatrix(app.middlegroundCamera.combined);
		orbit.setColor(255, 0, 255, 1);
		if(app.bullets != null && app.bullets.size()>0){
			for(int i=0; i<app.bullets.size(); i++){
				orbit.circle(app.bullets.get(i).body.getWorldCenter().x, app.bullets.get(i).body.getWorldCenter().y,  app.bullets.get(i).radius);
			}
		}
		orbit.end();

		batchBackground.begin();
		batchBackground.setProjectionMatrix(app.middlegroundCamera.combined);

		nebulaSprite.setX((IntoTheVoid.stageWidth / 2 - nebulaSprite.getWidth() / 2) - (player.ship.body.getPosition().x / 10));
		nebulaSprite.setY((IntoTheVoid.stageHeight / 2 - nebulaSprite.getWidth() / 2) - player.ship.body.getPosition().y / 10);
		nebulaSprite.draw(batchBackground);

		earth.update(batchBackground);
		//valhalla.update(batchBackground);
		moon.update(batchBackground);
		enemy1.update(batchBackground);

		player.update(batchBackground, delta);

		//****** Engine effect *********/
		controlHandler.setRunning(false);
		//******************************/

		for (int i = 0; i < cometCount; i++) {
			astSprite.draw(batchBackground);
			astSprite.setBounds(0,0, (float)comet[i].getUserData()*2, (float)comet[i].getUserData()*2);
			astSprite.setX(comet[i].getPosition().x - (astSprite.getWidth() / 2));
			astSprite.setY(comet[i].getPosition().y - (astSprite.getHeight() / 2));
			astSprite.setOriginCenter();
			astSprite.setRotation(MathUtils.radiansToDegrees*comet[i].getAngle());
		}

        /*
        if(player.bullets != null && player.bullets.size()>0){
            for(int i=0; i<player.bullets.size(); i++){
                player.bullets.get(i).trackBullets(batchBackground);
            }
        }
        */



		if (!this.explodingList.isEmpty()){
            /*
            for(Bullet bullet : this.explodingList){              //ConcurrentModificationException
                if (bullet.explode(delta, batchBackground)){
                    explodingList.remove(bullet);
                }
            }
            */

			//explodingList.removeIf(bullet -> !bullet.explode(delta, batchBackground));
		}


		player.removeBullet(delta);

		batchBackground.end();
		sunLight.setPosition(player.ship.body.getPosition().x - app.middlegroundCamera.viewportWidth*app.middlegroundCamera.zoom, player.ship.body.getPosition().y + app.middlegroundCamera.viewportHeight*app.middlegroundCamera.zoom);
		sunLight.setDistance(80*app.middlegroundCamera.zoom);

		RadialPlanetGravity.apply(space.planetVector, space.debrisVector);

		rayHandler.setCombinedMatrix(app.middlegroundCamera);
		postProcessor.render();
		rayHandler.updateAndRender();

		logger.log();
		renderer.render(world, app.middlegroundCamera.combined);
		world.step(1/60f, 12, 6);
	}

	@Override
	public void hide() {

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {
		postProcessor.rebind();
	}

	@Override
	public void dispose() {
		postProcessor.dispose();
	}

	// Input handlers.
	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.NUM_1) {
			if (shipLight.isActive()) {
				shipLight.setActive(false);
			} else {
				shipLight.setActive(true);
			}
		}

		if (keycode == Keys.NUM_2) {
			if (sunLight.isActive()) {
				sunLight.setActive(false);
			} else {
				sunLight.setActive(true);
			}
		}

		if (keycode == Keys.NUM_3) {
			app.setScreen(app.earth);
		}

		if (keycode == Keys.NUM_4) {
			if (renderer.isDrawBodies()) {
				renderer.setDrawBodies(false);
				renderer.setDrawAABBs(false);
				renderer.setDrawContacts(false);
				renderer.setDrawInactiveBodies(false);
				renderer.setDrawJoints(false);
				renderer.setDrawVelocities(false);
				postProcessor.setEnabled(false);
				debugLines = false;

			} else {
				renderer.setDrawBodies(true);
				renderer.setDrawAABBs(false);
				renderer.setDrawContacts(true);
				renderer.setDrawInactiveBodies(true);
				renderer.setDrawJoints(true);
				postProcessor.setEnabled(true);
				renderer.setDrawVelocities(true);
				debugLines = true;
			}
		}

		if (keycode == Keys.NUM_5) {
			jumping = true;
		}

		if (keycode == Keys.M) {
			app.map.cameFrom = "alpha";
			Gdx.input.setInputProcessor(app.map);
			app.setScreen(app.map);
		}

		if (keycode == Keys.ESCAPE) {
			app.setScreen(app.mainMenu);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if(zoomAmount >= app.minZoom && zoomAmount <= app.maxZoom){
			zoomAmount += amount;
			if(zoomAmount < app.minZoom){
				zoomAmount = app.minZoom;
			}
			else if(zoomAmount > app.maxZoom){
				zoomAmount = app.maxZoom;
			}
		}
		return false;
	}

	private void createCollisionListener() {
		world.setContactListener(new ContactListener() {

			@Override
			public void beginContact(Contact contact) {
				Fixture fixtureA = contact.getFixtureA();
				Fixture fixtureB = contact.getFixtureB();
				Gdx.app.log("beginContact", "between " + fixtureA.getBody().getPosition().toString() + " and " + fixtureB.getBody().getPosition().toString());

				Body b1 = fixtureA.getBody();
				Body b2 = fixtureB.getBody();

				Object o1 = b1.getUserData();
				Object o2 = b2.getUserData();

				Gdx.app.log("Object1 ", "Object's class: " + o1.toString());
				Gdx.app.log("Object2 ", "Object's class: " + o2.toString());

				if(o1.getClass() == Bullet.class){
					Bullet bullet1 = (Bullet)o1;
					Gdx.app.log("Bullet1", "index: " + bullet1.index);
					Gdx.app.log("Size", "" + app.bullets.size());
					if (app.bullets.size() > bullet1.index) {
						app.bullets.get(bullet1.index).lifeTimer = 0;
						app.getSoundManager().play(SoundHandler.ITVSound.Explosion2);
						explodingList.add(app.bullets.get(bullet1.index));
						//explosionAnimation.setExplosionCoordinates(explodingList, player.bullets.get(bullet1.index).body.getPosition().x, player.bullets.get(bullet1.index).body.getPosition().y);
					}
				}

				if(o2.getClass() == Bullet.class) {
					Bullet bullet2 = (Bullet) o2;
					Gdx.app.log("Bullet2", "index: " + bullet2.index);
					Gdx.app.log("Size", "" + app.bullets.size());

					if (app.bullets.size() > bullet2.index) {
						app.bullets.get(bullet2.index).lifeTimer = 0;

						explodingList.add(app.bullets.get(bullet2.index));
						//explosionAnimation.setExplosionCoordinates(explodingList, player.bullets.get(bullet2.index).body.getPosition().x, player.bullets.get(bullet2.index).body.getPosition().y);
					}
				}
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

		});
	}
}