package com.onurzubari.games.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Zubari on 27.03.2017.
 */
public class ExplosionTextureRegion {
    public Texture explosionSheet;
    private TextureRegion[] explosionFrames;

    public ExplosionTextureRegion(){
        explosionSheet = new Texture(Gdx.files.internal("explosion.png"));
        int column = 8;
        int row = 6;
        TextureRegion[][] tmp = TextureRegion.split(explosionSheet,
                explosionSheet.getWidth() / column,
                explosionSheet.getHeight() / row);

        explosionFrames = new TextureRegion[column * row];
        int index = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                explosionFrames[index++] = tmp[i][j];
            }
        }
    }

    public TextureRegion[] getRegion(){
        return explosionFrames;
    }

}
