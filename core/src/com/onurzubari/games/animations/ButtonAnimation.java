package com.onurzubari.games.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.onurzubari.games.IntoTheVoid;

/**
 * Created by Zubari on 27.03.2017.
 * Into the void.
 */
public class ButtonAnimation {

    private Animation buttonAnimation;
    private float time;

    public ButtonAnimation(){
        buttonAnimation = new Animation(1/MathUtils.random(30f,45f), IntoTheVoid.buttonTextureRegion.getRegion());
        time = MathUtils.random(1f,5f);
    }

    public boolean drawButton(float time, SpriteBatch spriteBatch, float posX, float posY){
        this.time += time;
        TextureRegion currentFrame = buttonAnimation.getKeyFrame(this.time, true);
        spriteBatch.draw(currentFrame, posX, posY - (currentFrame.getRegionHeight())/2, currentFrame.getRegionWidth(), currentFrame.getRegionHeight());
        return !buttonAnimation.isAnimationFinished(this.time);
    }
}
