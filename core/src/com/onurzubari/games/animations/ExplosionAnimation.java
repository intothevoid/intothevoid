package com.onurzubari.games.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.onurzubari.games.IntoTheVoid;

/**
 * Created by Zubari on 27.03.2017.
 * Into the void.
 */
public class ExplosionAnimation {

    private Animation explosionAnimation;
    private float rotation;
    TextureRegion currentFrame;
    float time = 0, posX, posY;
    SpriteBatch spriteBatch;

    public ExplosionAnimation(SpriteBatch spriteBatch, float posX, float posY){
        explosionAnimation = new Animation(1/60f, IntoTheVoid.explosionTextureRegion.getRegion());
        rotation = MathUtils.random(0f,360f);
        currentFrame = new TextureRegion();
        this.posX = posX;
        this.posY = posY;
        this.spriteBatch = spriteBatch;
    }

    public void drawExplosion(){
        this.time += Gdx.graphics.getDeltaTime();
        float scale = 0.002f;
        currentFrame = explosionAnimation.getKeyFrame(this.time, false);
        spriteBatch.draw(currentFrame, posX - currentFrame.getRegionWidth()/2, posY - currentFrame.getRegionHeight()/2, currentFrame.getRegionWidth()/2, currentFrame.getRegionHeight()/2, currentFrame.getRegionWidth(), currentFrame.getRegionHeight(), scale, scale, rotation);
    }

    public void setExplosionCoordinates(float x, float y){
        this.posX = x;
        this.posY = y;
    }

    public boolean isAnimationFinished(){
        return explosionAnimation.isAnimationFinished(this.time);
    }
}
