package com.onurzubari.games.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Zubari on 27.03.2017.
 */
public class ButtonTextureRegion {
    public Texture buttonSheet;
    private TextureRegion[] buttonFrames;

    public ButtonTextureRegion(){
        buttonSheet = new Texture(Gdx.files.internal("buttons/buttonSequence.png"));
        buttonSheet.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        int column = 10;
        int row = 9;
        TextureRegion[][] tmp = TextureRegion.split(buttonSheet,
                buttonSheet.getWidth() / column,
                buttonSheet.getHeight() / row);

        buttonFrames = new TextureRegion[column * row];
        int index = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                buttonFrames[index++] = tmp[i][j];
            }
        }
    }

    public TextureRegion[] getRegion(){
        return buttonFrames;
    }

}
