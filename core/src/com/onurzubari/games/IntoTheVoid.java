package com.onurzubari.games;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.onurzubari.games.animations.ButtonTextureRegion;
import com.onurzubari.games.animations.ExplosionTextureRegion;
import com.onurzubari.games.objects.Bullet;
import com.onurzubari.games.screens.*;
import com.onurzubari.games.utils.AssetHandler;
import com.onurzubari.games.utils.MusicHandler;
import com.onurzubari.games.utils.PreferencesManager;
import com.onurzubari.games.utils.SoundHandler;

import java.util.ArrayList;

public class IntoTheVoid extends Game {
	
	//Global Variables.
	public OrthographicCamera foregroundCamera;
	public OrthographicCamera backgroundCamera;
	public OrthographicCamera middlegroundCamera;

	public String userName;
	public String shipName;

	public static float stageWidth;
	public static float stageHeight;
	public static float windowWidth;
	public static float windowHeight;
	public static final String version = "v0.06 Alpha";

	public MainMenu mainMenu;
	private SplashScreen splashScreen;
	public Solar solar;
	public Centauri centauri;
	public Earth earth;
	public UniverseMap map;
	public Sound thrust;
	public AssetHandler assetHandler;
	public ArrayList<Bullet> bullets;

	private PreferencesManager preferencesManager;
	//Sound
	private MusicHandler musicHandler;
	private SoundHandler soundHandler;

	public static ExplosionTextureRegion explosionTextureRegion;
	public static ButtonTextureRegion buttonTextureRegion;

	public int minZoom = 1;
	public int maxZoom = 5;

	// Getters
	public PreferencesManager getPreferencesManager() {
		return preferencesManager;
	}
	public MusicHandler getMusicManager() {
		return musicHandler;
	}
	public SoundHandler getSoundManager() {
		return soundHandler;
	}
	public AssetHandler getAssetHandler() {
		return assetHandler;
	}
	
	@Override
	public void create () {
		windowWidth = Gdx.graphics.getWidth();
		windowHeight = Gdx.graphics.getHeight();

		stageWidth = 20f;
		stageHeight = (windowHeight / windowWidth) * stageWidth;

		foregroundCamera = new OrthographicCamera(stageWidth, stageHeight);
		backgroundCamera = new OrthographicCamera(stageWidth, stageHeight);
		middlegroundCamera = new OrthographicCamera(stageWidth, stageHeight);

		foregroundCamera.translate(stageWidth / 2, stageHeight / 2, 0);
		backgroundCamera.translate(stageWidth / 2, stageHeight / 2, 0);
		middlegroundCamera.translate(stageWidth / 2, stageHeight / 2, 0);

		foregroundCamera.update();
		backgroundCamera.update();
		middlegroundCamera.update();

		assetHandler = new AssetHandler();

		explosionTextureRegion = new ExplosionTextureRegion();
		buttonTextureRegion = new ButtonTextureRegion();

		mainMenu = new MainMenu(this);
		splashScreen = new SplashScreen(this);
		solar = new Solar(this);
		centauri = new Centauri(this);
		earth = new Earth(this);
		map = new UniverseMap(this);

		// create the preferences manager
		preferencesManager = new PreferencesManager();

		// create the music manager
		musicHandler = new MusicHandler();
		musicHandler.setVolume( preferencesManager.getVolume() );
		musicHandler.setEnabled( preferencesManager.isMusicEnabled() );

		// create the sound manager
		soundHandler = new SoundHandler();
		soundHandler.setVolume( preferencesManager.getVolume() );
		soundHandler.setEnabled( preferencesManager.isSoundEnabled() );

		thrust = Gdx.audio.newSound(Gdx.files.internal("sound/thrust.mp3"));
		thrust.loop();
		thrust.pause(0);
		thrust.setVolume(0, 0.01f);

		bullets = new ArrayList<>();
		
		this.setScreen(this.mainMenu);
		//this.setScreen(this.solar);
		//Gdx.input.setInputProcessor(solar);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		this.getScreen().dispose();
		solar.dispose();
		centauri.dispose();
		earth.dispose();
		splashScreen.dispose();
		musicHandler.dispose();
		soundHandler.dispose();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}
}