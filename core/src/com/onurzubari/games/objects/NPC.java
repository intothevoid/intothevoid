package com.onurzubari.games.objects;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.utils.BodyEditorLoader;
import com.onurzubari.games.utils.SoundHandler;

import java.util.ArrayList;

public class NPC {

	public Body body;
	private float width;
	private float height;
	private float positionX;
	private float positionY;
	private float shipAngle;
	public float hull = 30, shield = 30, currentHull = 30, currentShield = 30;
	private float maxSpeed;
	private float maxRotationalSpeed;
	private float fuelCapacity = 3000;
	private float currentFuel = 3000;
	private float reloadTimer = 0;
	private ArrayList<Vector2> gunPort;
	private ArrayList<Vector2> engines;
	private World world;
	private Space space;
	public Boolean alive = true;
	public Boolean destroyed = false;
	private Boolean engineRunning = false;
	public Sprite spriteNPC;
	private BodyEditorLoader loader;
	private Vector2 shipPos;
	private Vector2 shipOrgin;
	private IntoTheVoid app;
	private float engineForce;
	private float followDistance;
	private ConeLight shipLight;
	private ParticleEffect engineEffect;
	public double expValue = 400;
	public String status = "";
	public boolean hostile = false;
	
	public NPC(String role, float width, float height, float positionX, float positionY, World world, Space space, IntoTheVoid app){
		this.app = app;
		this.width = width;
		this.height = height;
		this.positionX = positionX;
		this.positionY = positionY;
		this.world = world;
		this.space = space;
		this.gunPort = new ArrayList<>();
		this.engines = new ArrayList<>();

		this.engineForce = 2f;
		this.maxSpeed = 12;
		this.followDistance = MathUtils.random(IntoTheVoid.stageHeight/2, IntoTheVoid.stageHeight);
		this.maxRotationalSpeed = 1f;
		loader = new BodyEditorLoader(Gdx.files.internal("bodies/enemy.json"));
		engineEffect = new ParticleEffect();
		engineEffect.load(Gdx.files.internal("engine.p"), Gdx.files.internal(""));
		engineEffect.scaleEffect(this.width/IntoTheVoid.stageWidth);
		createShip(Gdx.files.internal("enemy.png"));
		space.debrisVector.add(this.body);
	}
	
	public void createShip(FileHandle file){
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(this.positionX, this.positionY);
		this.body = world.createBody(playerDef);
		this.body.setUserData(this);
		FixtureDef playerFixture = new FixtureDef();
		playerFixture.density = 1;
		this.addGunPorts();
		loader.attachFixture(this.body, "enemy", playerFixture, this.width*2);
		Texture textureNPC = new Texture(file);
		spriteNPC = new Sprite(textureNPC, 0, 0, textureNPC.getWidth(), textureNPC.getHeight());
		spriteNPC.setBounds(0, 0, this.width*2, this.height*2);
		shipOrgin = loader.getOrigin("enemy", this.width*2).cpy();
	}

	public void attachLights(RayHandler rayHandler){
		/*shipLight = new ConeLight(rayHandler, 200, Color.GRAY, IntoTheVoid.stageHeight, this.body.getPosition().x +2, this.body.getPosition().y, shipAngle, 10);
		shipLight.attachToBody(this.body);
		shipLight.setIgnoreAttachedBody(true);*/
	}

	public void takeDamage(int damage){
		if (this.currentShield > 0){
			currentShield -= damage;
		} else if (this.currentHull > 0){
			currentHull -= damage;
			if (this.currentHull <= 0) {
				this.alive = false;
			}
		}
	}

	public void destroyNPC(){
		//this.shipLight.remove(true);
		this.space.debrisVector.remove(this.body);
		this.destroyed = true;
	}

	public void update(SpriteBatch batch, Body target, float delta){
		if (target != null){
			followTarget(target);
		}

		if(this.reloadTimer > 0){
			this.reloadTimer -= delta;
		}

		if(hostile){
			if(this.reloadTimer <= 0){
				shoot(batch);
			}
		}

		shipPos = this.body.getPosition().sub(shipOrgin);
		spriteNPC.setPosition(shipPos.x, shipPos.y);
		spriteNPC.setOrigin(shipOrgin.x, shipOrgin.y);
		spriteNPC.setRotation(this.body.getAngle() * MathUtils.radiansToDegrees);
		spriteNPC.draw(batch);


		engineEffect.update(delta);
		this.updateEnginePositions();
		engineEffect.setPosition(this.engines.get(0).x, this.engines.get(0).y);
		if (engineRunning) {
			engineEffect.start();
		} else {
			engineEffect.allowCompletion();
		}
		engineEffect.draw(batch);
		engineRunning = false;
	}

	public void proximity(){

	}
	
	private Vector2 getGunPorts(int index){
		updateGunPortPositions();
		return gunPort.get(index);
	}

	private void followTarget(Body target){
		float dx = target.getPosition().x - this.body.getPosition().x;
		float dy = target.getPosition().y - this.body.getPosition().y;
		Vector2 distance = target.getPosition().sub(this.body.getPosition());

		float targetAngle = target.getPosition().sub(this.body.getPosition()).angle();
		shipAngle = Math.floorMod((long) (MathUtils.radiansToDegrees * this.body.getAngle()), 360);

		float angleDifference = targetAngle - shipAngle;
		angleDifference += (angleDifference>180) ? -360 : (angleDifference<-180) ? 360 : 0;

		if(angleDifference > 1){
			this.body.setAngularDamping(0);
			this.body.applyTorque(this.body.getInertia() * (maxRotationalSpeed - this.body.getAngularVelocity()) / 0.5f, true);
		} else if (angleDifference < -1){
			this.body.setAngularDamping(0);
			this.body.applyTorque(this.body.getInertia() * (-maxRotationalSpeed - this.body.getAngularVelocity()) / 0.5f, true);
		} else {
			this.body.setAngularDamping(2);
		}

		if(distance.len() > this.followDistance){
			chaseTarget(dx, dy);
			this.body.setLinearDamping(0.2f);
			//this.shipLight.setColor(Color.RED);
			this.status = "chasing";
		} else {
			equalizeVelocity(target.getLinearVelocity());
			this.body.setLinearDamping(0);
			//this.shipLight.setColor(Color.GRAY);
			this.status = "close";
		}

		//Speed limit
		Vector2 velocity = this.body.getLinearVelocity();
		float speed = velocity.len();
		if(speed > this.maxSpeed){
			this.body.setLinearVelocity(velocity.scl(this.maxSpeed/speed));
		}

		//Rotation limit
		float angularVelocity = this.body.getAngularVelocity();
		if(angularVelocity > this.maxRotationalSpeed){
			this.body.setAngularVelocity(this.maxRotationalSpeed);
		} else if(angularVelocity < -this.maxRotationalSpeed){
			this.body.setAngularVelocity(-this.maxRotationalSpeed);
		}
	}

	private void chaseTarget(float dx, float dy){
		if (currentFuel > 0){
			float angle = (float)Math.atan2(dy, dx);
			this.body.applyForceToCenter(engineForce/2 * MathUtils.cos(angle), engineForce/2 * MathUtils.sin(angle),  true);
			this.currentFuel -= 0.01;
			this.app.thrust.resume(0);
		}
	}

	private void shoot(SpriteBatch batch){
		float shipAngle = this.body.getAngle();
		for(int i=0; i<this.gunPort.size(); i++){
			Bullet bullet = new Bullet(world, this.getGunPorts(i).x, this.getGunPorts(i).y, app, batch);
			bullet.body.setLinearVelocity(this.body.getLinearVelocity().x+MathUtils.cos(shipAngle)*60, this.body.getLinearVelocity().y+MathUtils.sin(shipAngle)*60);
			app.bullets.add(bullet);
			bullet.index = app.bullets.indexOf(bullet);
			this.body.applyForce(MathUtils.cos(shipAngle)*-2f,MathUtils.sin(shipAngle)*-2f, this.body.getPosition().x, this.body.getPosition().y, true);
			app.getSoundManager().play(SoundHandler.ITVSound.Shot);
			this.reloadTimer = 0.6f;
		}
	}

	private void equalizeVelocity(Vector2 velocity){
		this.body.applyForceToCenter(velocity.scl(engineForce/10).sub(this.body.getLinearVelocity().scl(engineForce/10)), true);
	}
	
	private void addGunPorts(){
		this.gunPort.add(new Vector2(0,0));
		this.gunPort.add(new Vector2(0,0));
		this.engines.add(new Vector2(0,0));
	}
	
	private void updateGunPortPositions(){
		shipAngle = body.getAngle();
		Vector2 firstPort = new Vector2(this.body.getWorldPoint(new Vector2(0,height/2)));
		Vector2 secondPort = new Vector2(this.body.getWorldPoint(new Vector2(0,-height/2)));

		this.gunPort.set(0, firstPort.add(this.width/2*MathUtils.cos(shipAngle), this.height/2*MathUtils.sin(shipAngle)));
		this.gunPort.set(1,  secondPort.add(this.width/2*MathUtils.cos(shipAngle), this.height/2*MathUtils.sin(shipAngle)));
	}
	public void updateEnginePositions(){
		shipAngle = body.getAngle();
		this.engines.set(0, new Vector2(this.body.getWorldCenter().sub(this.width*MathUtils.cos(shipAngle)/2, this.height*MathUtils.sin(shipAngle)/2)));
	}
}