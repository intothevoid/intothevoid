package com.onurzubari.games.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.onurzubari.games.utils.CreateBody;

public class Moon {
	Space space;
	public Planet parent;
	public float radius;
	public World world;
	public Body body;
	public float posX, posY, angle, targetAngle;
	Vector2 planetCoord = new Vector2();
	public float rotateSpeed = 0.01875f, rotateAngle = 90;
	Texture moonTexture;
	public Sprite moonSprite;
	
	public Moon(Space space, Planet parent, float posX, float posY, float radius){
		this.parent = parent;
		this.space = space;
		this.radius = radius;
		this.posX = posX;
		this.posY = posY;
		this.world = space.world;
		this.body = CreateBody.createPlanet(space, parent.body.getPosition().x+MathUtils.cosDeg(0)*parent.radius*2, parent.body.getPosition().y+MathUtils.sinDeg(0)*parent.radius*2, radius);
		this.body.setType(BodyDef.BodyType.KinematicBody);
		this.body.setUserData(this);
	}
	
	public void setTexture(FileHandle file){
		moonTexture = new Texture(file);
		moonSprite = new Sprite(moonTexture, 0, 0, moonTexture.getWidth(), moonTexture.getHeight());
		moonSprite.setBounds(0, 0, this.radius*2, this.radius*2);
	}
	
	public void update(Batch batch){
		moonSprite.setX(this.body.getPosition().x - this.radius);
		moonSprite.setY(this.body.getPosition().y - this.radius);
		moonSprite.setOriginCenter();
		moonSprite.setRotation(MathUtils.radiansToDegrees*this.body.getAngle());
		moonSprite.draw(batch);
	}
	
	public void rotateMoon(){
		posX = parent.body.getPosition().x+MathUtils.cosDeg(rotateAngle)*parent.radius*2;
		posY = parent.body.getPosition().y+MathUtils.sinDeg(rotateAngle)*parent.radius*2;
		this.angle = body.getAngle();
		this.planetCoord = new Vector2(parent.body.getPosition().x - body.getPosition().x,parent.body.getPosition().y - body.getPosition().y);
		targetAngle = MathUtils.atan2(-planetCoord.x, planetCoord.y);
		this.body.setTransform(posX, posY, targetAngle);
		rotateAngle = (rotateAngle+rotateSpeed) % 360;
	}
}
