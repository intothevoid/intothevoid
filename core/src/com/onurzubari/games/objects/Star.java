package com.onurzubari.games.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.onurzubari.games.utils.CreateBody;

public class Star {
	public float radius;
	public World world;
	public Body body;
	Texture planetTexture;
	public Sprite planetSprite;
	public String name;
	
	public Star(World world, float posX, float posY, float radius, String name){
		this.radius = radius;
		this.world = world;
		this.body = CreateBody.createStar(world, posX, posY, radius);
		this.name = name;
	}
	
	public void setTexture(FileHandle file){
		planetTexture = new Texture(file);
		//planetTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		planetSprite = new Sprite(planetTexture, 0, 0, planetTexture.getWidth(), planetTexture.getHeight());
		planetSprite.setBounds(0, 0, this.radius*2, this.radius*2);
		planetSprite.setOriginCenter();
		planetSprite.setRotation(MathUtils.random(0,360));
	}
	
	public void update(Batch batch){
		planetSprite.setX(this.body.getPosition().x - this.radius);
		planetSprite.setY(this.body.getPosition().y - this.radius);
		planetSprite.draw(batch);
	}
}
