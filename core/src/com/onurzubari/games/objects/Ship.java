package com.onurzubari.games.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import java.util.ArrayList;

public class Ship {
	public Body body;
	public float width, height, shipAngle, hull = 100, shield = 100, currentHull = 100, currentShield = 100, maxSpeed, maxRotationalSpeed, fuelCapacity = 300, currentFuel = 100, reloadTimer = 0, reloadTime = 0.2f;
	int price;
	ArrayList<Vector2> gunPort;
	ArrayList<Vector2> engines;
	ParticleEffect engineEffect;
	public Boolean engineRunning = false;
	
	Ship(float width, float height){
		this.width = width;
		this.height = height;
		this.gunPort = new ArrayList<Vector2>();
		this.engines = new ArrayList<Vector2>();
		this.maxSpeed = 10;
		this.maxRotationalSpeed = 2;
		engineEffect = new ParticleEffect();
		engineEffect.load(Gdx.files.internal("engine.p"), Gdx.files.internal(""));
		engineEffect.scaleEffect(this.width/20);
	}
	
	Vector2 getGunPorts(int index){
		updateGunPortPositions();
		return gunPort.get(index);
	}

	void addPorts(){
		this.gunPort.add(new Vector2(0,0));
		this.gunPort.add(new Vector2(0,0));
		this.engines.add(new Vector2(0,0));
	}
	
	private void updateGunPortPositions(){
		shipAngle = body.getAngle();
		Vector2 firstPort = new Vector2(this.body.getWorldPoint(new Vector2(0,height/2)));
		Vector2 secondPort = new Vector2(this.body.getWorldPoint(new Vector2(0,-height/2)));

		this.gunPort.set(0, firstPort.add(this.width/2*MathUtils.cos(shipAngle), this.height/2*MathUtils.sin(shipAngle)));
		this.gunPort.set(1,  secondPort.add(this.width/2*MathUtils.cos(shipAngle), this.height/2*MathUtils.sin(shipAngle)));
	}

	void updateEnginePositions(){
		shipAngle = body.getAngle();
		this.engines.set(0, new Vector2(this.body.getWorldCenter().sub(this.width*MathUtils.cos(shipAngle)/2, this.height*MathUtils.sin(shipAngle)/2)));
	}

	public void takeDamage(int damage){
		if (this.currentShield > 0){
			currentShield -= damage;
		} else if (this.currentHull > 0){
			currentHull -= damage;
		}
	}
}