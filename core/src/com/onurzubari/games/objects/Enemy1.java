package com.onurzubari.games.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.onurzubari.games.utils.BodyEditorLoader;

import java.util.ArrayList;

public class Enemy1 {

	public Body body;
	public float width, height, positionX, positionY, shipAngle, hull = 100, shield = 100, currentHull = 100, currentShield = 60, maxSpeed, maxRotationalSpeed, fuelCapacity = 300, currentFuel = 200, reloadTimer = 0, reloadTime = 0.2f;
	int price;
	ArrayList<Vector2> gunPort;
	public ArrayList<Vector2> engines;
	World world;
	
	Texture enemyTexture;
	public Sprite enemySprite;
	BodyEditorLoader loader;
	Vector2 shipPos;
	Vector2 shipOrgin;
	boolean alive = true;
	
	public Enemy1(float width, float height, float positionX, float positionY, World world){
		this.width = width;
		this.height = height;
		this.positionX = positionX;
		this.positionY = positionY;
		this.world = world;
		this.gunPort = new ArrayList<>();
		this.engines = new ArrayList<>();
		this.maxSpeed = 50;
		this.maxRotationalSpeed = 2;
		loader = new BodyEditorLoader(Gdx.files.internal("bodies/asd.json"));
	}
	
	public void createShip(FileHandle file){
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(this.positionX, this.positionY);
		this.body = world.createBody(playerDef);
		this.body.setUserData(this);
		FixtureDef playerFixture = new FixtureDef();
		playerFixture.density = 1;
		this.addGunPorts();
		loader.attachFixture(this.body, "Name", playerFixture, width*2);
		enemyTexture = new Texture(file);
		enemySprite = new Sprite(enemyTexture, 0, 0, enemyTexture.getWidth(), enemyTexture.getHeight());
		enemySprite.setBounds(0, 0, this.width*2, this.height*2);
		shipOrgin = loader.getOrigin("Name", this.width*2).cpy();
	}

	public void takeDamage(int damage){
		if (this.currentShield > 0){
			currentShield -= damage;
		} else if (this.currentHull > 0){
			currentHull -= damage;
			if (this.currentHull <= 0) {
				this.alive = false;
			}
		}
	}
	
	public void update(Batch batch){
		shipPos = this.body.getPosition().sub(shipOrgin);
		enemySprite.setPosition(shipPos.x, shipPos.y);
		enemySprite.setOrigin(shipOrgin.x, shipOrgin.y);
		enemySprite.setRotation(this.body.getAngle() * MathUtils.radiansToDegrees);
		enemySprite.draw(batch);
	}
	
	public Vector2 getGunPorts(int index){
		updateGunPortPositions();
		return gunPort.get(index);
	}
	
	public void addGunPorts(){
		this.gunPort.add(new Vector2(0,0));
		this.gunPort.add(new Vector2(0,0));
		this.engines.add(new Vector2(0,0));
	}
	
	public void updateGunPortPositions(){
		this.gunPort.set(0, new Vector2(body.getWorldPoint(new Vector2(0.5f/5,-2.2f/4)).x+(MathUtils.cos(shipAngle)), body.getWorldPoint(new Vector2(0.5f/5,-2.2f/4)).y+(MathUtils.sin(shipAngle))));
		this.gunPort.set(1, new Vector2(body.getWorldPoint(new Vector2(0.5f/5,2.2f/4)).x+(MathUtils.cos(shipAngle)), body.getWorldPoint(new Vector2(0.5f/5,2.2f/4)).y+(MathUtils.sin(shipAngle))));
	}
	
	public void updateEnginePositions(){
		shipAngle = body.getAngle();
		this.engines.set(0, new Vector2(body.getWorldPoint(new Vector2(-2.2f,0)).x+(MathUtils.cos(shipAngle)), body.getWorldPoint(new Vector2(-2.2f,0)).y+(MathUtils.sin(shipAngle))));

	}
}