package com.onurzubari.games.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.onurzubari.games.utils.CreateBody;

public class Planet {
	public float radius;
	public World world;
	public Body body;
	Texture planetTexture;
	Sprite planetSprite;
	
	public Planet(Space space, float posX, float posY, float radius){
		this.radius = radius;
		this.world = space.world;
		this.body = CreateBody.createPlanet(space, posX, posY, radius);
		this.body.setUserData(this);
	}
	
	public void setTexture(FileHandle file){
		planetTexture = new Texture(file, true);
		//planetTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		planetSprite = new Sprite(planetTexture, 0, 0, planetTexture.getWidth(), planetTexture.getHeight());
		planetSprite.setBounds(0, 0, this.radius*2, this.radius*2);
	}
	
	public void update(Batch batch){
		planetSprite.setX(this.body.getPosition().x - this.radius);
		planetSprite.setY(this.body.getPosition().y - this.radius);
		planetSprite.draw(batch);
	}
}
