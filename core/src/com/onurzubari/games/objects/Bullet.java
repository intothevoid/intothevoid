package com.onurzubari.games.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.animations.ExplosionAnimation;
import com.onurzubari.games.utils.FixtureDefinition;

public class Bullet {
	
	public Body body;
	public int damage = 1;
	private float lifeTime;
	public float lifeTimer;
	public float radius;
	public int index;
	ExplosionAnimation animation;
	public boolean exploding = false;

	private Vector2 position;
	private Sprite bulletSprite;
	World world;
	
	public Bullet(World world, float positionX, float positionY, IntoTheVoid app, SpriteBatch batch){
		animation = new ExplosionAnimation(batch, positionX, positionY);
		this.lifeTime = 0;
		this.lifeTimer = 4;
		this.radius = 0.05f;
		this.world = world;
        BodyDef bulletDef = new BodyDef();
        bulletDef.type = BodyType.DynamicBody;
        bulletDef.position.set(positionX, positionY);
        bulletDef.bullet = true;
        body = world.createBody(bulletDef);
        body.setUserData(this);
        CircleShape playerShape = new CircleShape();
        playerShape.setRadius(this.radius);
        FixtureDef playerFixture = FixtureDefinition.create(playerShape, 1);
        playerFixture.friction = 0;
        playerFixture.density = 0.1f;
        playerFixture.restitution = 0.2f;
        body.createFixture(playerFixture);
		Texture bulletTexture = app.getAssetHandler().getAssetManager().get("bullet.png", Texture.class);
		bulletSprite = new Sprite(bulletTexture, 0,0, bulletTexture.getWidth(), bulletTexture.getHeight());
		bulletSprite.setBounds(0, 0, this.radius*10, this.radius*10);
	}
	
	public boolean isBulletTimedOut(float deltaTime){
		this.lifeTime += deltaTime;
		if(lifeTime >= lifeTimer){
			return true;
		}
		return false;
	}

	public void update(SpriteBatch batch){
		position = this.body.getPosition();
		bulletSprite.setPosition(position.x - this.radius*5, position.y - this.radius*5);
		bulletSprite.setOriginCenter();
		bulletSprite.setRotation(this.body.getLinearVelocity().angle());
		bulletSprite.draw(batch);
	}

	public void updateExplosion(){
        //Gdx.app.log("Exploding", "bullet " + this.index);
        animation.drawExplosion();
    }

	public void explode(){
        animation.setExplosionCoordinates(this.body.getPosition().x, this.body.getPosition().y);
        this.exploding = true;
        this.lifeTimer = 0;
	}
}
