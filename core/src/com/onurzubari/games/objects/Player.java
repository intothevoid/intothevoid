package com.onurzubari.games.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;
import com.onurzubari.games.IntoTheVoid;
import com.onurzubari.games.utils.BodyEditorLoader;
import com.onurzubari.games.utils.SoundHandler;

import java.util.Timer;

public class Player {
	
	public Ship ship;
	public String name;
	public int points = 0;
	private World world;
	private float width, height, positionX, positionY;
	
	private BodyEditorLoader loader;
	private Vector2 shipOrigin;

	private Sprite enemySprite;
	private SpriteBatch batch;
	private IntoTheVoid app;

	public double exp = 0;
	public int level = 1;
	public double nextLevelExp = 1000;

	private long startTime = 0;
	
	public Player(IntoTheVoid app){
		loader = new BodyEditorLoader(Gdx.files.internal("bodies/enemy.json"));
		this.app = app;
	}

	public void levelUp(double exp){
		this.level++;
		this.exp = (this.exp + exp) - nextLevelExp;
		nextLevelExp = Math.pow(nextLevelExp, 1.1);  //Next level exp progression ratio.
		System.out.println("Next: " + nextLevelExp);
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setWidth(float width){
		this.width = width;
	}
	
	public float getWidth(){
		return this.width;
	}
	
	public void setHeight(float height){
		this.height = height;
	}
	
	public float getHeight(){
		return this.height;
	}
	
	public void setShip(){
		this.ship = new Ship(this.width, this.height);
	}
	
	public Ship getShip(){
		return this.ship;
	}
	
	public void setWorld(World world){
		this.world = world;
	}
	
	public World getWorld(){
		return this.world;
	}
	
	public void setPositionX(float positionX){
		this.positionX = positionX;
	}
	
	public float getPositionX(){
		return this.positionX;
	}
	
	public void setPositionY(float positionY){
		this.positionY = positionY;
	}
	
	public float getPositionY(){
		return this.positionY;
	}
	
	public void createPlayerShip(FileHandle file, SpriteBatch batch){
		this.batch = batch;
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(this.positionX, this.positionY);
		this.ship.body = world.createBody(playerDef);
		this.ship.body.setUserData(this);
		FixtureDef playerFixture = new FixtureDef();
		playerFixture.density = 1;
		loader.attachFixture(this.ship.body, "enemy", playerFixture, this.width*2);
		Texture enemyTexture = new Texture(file, true);
		//enemyTexture.setFilter(TextureFilter.Linear, TextureFilter.Nearest);
		enemySprite = new Sprite(enemyTexture, 0, 0, enemyTexture.getWidth(), enemyTexture.getHeight());
		enemySprite.setBounds(0, 0, this.width*2, this.height*2);
		shipOrigin = loader.getOrigin("enemy", this.width*2).cpy();
		ship.addPorts();
	}
	
	public void update(Batch batch, float delta){
		Vector2 shipPos = this.ship.body.getPosition().sub(shipOrigin);
		enemySprite.setPosition(shipPos.x, shipPos.y);
		enemySprite.setOrigin(shipOrigin.x, shipOrigin.y);
		enemySprite.setRotation(this.ship.body.getAngle() * MathUtils.radiansToDegrees);
		enemySprite.draw(batch);

		//Engine effect
		this.ship.engineEffect.update(delta);
		this.ship.updateEnginePositions();
		this.ship.engineEffect.setPosition(this.ship.engines.get(0).x, this.ship.engines.get(0).y);
		if (this.ship.engineRunning) {
			this.ship.engineEffect.start();
		} else {
			this.ship.engineEffect.allowCompletion();
		}
		this.ship.engineEffect.draw(batch);
		this.ship.engineRunning = false;

		if (TimeUtils.timeSinceNanos(startTime) > 1000000000 && this.ship.currentShield < this.ship.shield) {
			if(this.ship.currentShield + this.ship.shield/1000 > this.ship.shield){
				this.ship.currentShield = this.ship.shield;
			} else {
				this.ship.currentShield += this.ship.shield/1000;
			}
			startTime = TimeUtils.nanoTime();
		}
	}
	
	public void getPlayerPosition(){
		this.positionX = this.ship.body.getPosition().x;
		this.positionY = this.ship.body.getPosition().y;
	}

	public void shoot(){
		float shipAngle = ship.body.getAngle();
		for(int i=0; i<ship.gunPort.size(); i++){
			Bullet bullet = new Bullet(world, ship.getGunPorts(i).x, ship.getGunPorts(i).y, app, batch);
			bullet.body.setLinearVelocity(ship.body.getLinearVelocity().x+MathUtils.cos(shipAngle)*60, ship.body.getLinearVelocity().y+MathUtils.sin(shipAngle)*60);
			app.bullets.add(bullet);
			bullet.index = app.bullets.indexOf(bullet);
			this.ship.body.applyForce(MathUtils.cos(shipAngle)*-2f,MathUtils.sin(shipAngle)*-2f, this.ship.body.getPosition().x, this.ship.body.getPosition().y, true);
			app.getSoundManager().play(SoundHandler.ITVSound.Shot);
			this.ship.reloadTimer = this.ship.reloadTime;
		}
	}
	
	public void removeBullet(float deltaTime){
		if(app.bullets != null && app.bullets.size()>0){
			for(int i=0; i<app.bullets.size(); i++){
				if(app.bullets.get(i).exploding){
					if (app.bullets.get(i).body.getFixtureList().size > 0){
						app.bullets.get(i).body.destroyFixture(app.bullets.get(i).body.getFixtureList().first());
					}
					if(app.bullets.get(i).animation.isAnimationFinished()){
						app.bullets.get(i).world.destroyBody(app.bullets.get(i).body);
						app.bullets.remove(i);
						app.bullets.trimToSize();
						for(Bullet bullet : app.bullets){
							bullet.index = app.bullets.indexOf(bullet);
						}
					}
				} else if(app.bullets.get(i).isBulletTimedOut(deltaTime)){
					app.bullets.get(i).world.destroyBody(app.bullets.get(i).body);
					app.bullets.remove(i);
					app.bullets.trimToSize();
					for(Bullet bullet : app.bullets){
						bullet.index = app.bullets.indexOf(bullet);
					}
				}
			}
		}
	}
}
