package com.onurzubari.games.objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

public class Space {
	public World world;
	public ArrayList<Body> planetVector = new ArrayList<Body>();
	public ArrayList<Body> debrisVector = new ArrayList<Body>();
	
	public Space(){
		this.world = new World(new Vector2(0, 0), false);
	}
}
