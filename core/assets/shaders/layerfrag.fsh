#ifdef GL_ES
precision lowp float;
#define MED mediump
#else
#define MED
#endif

varying MED vec2 v_texCoords;

uniform float u_ambientColor;
uniform sampler2D u_texture0;
uniform sampler2D u_texture1;

void main() {
    vec4 foreground = texture2D(u_texture0, v_texCoords);
    vec4 light = texture2D(u_texture1, v_texCoords);

    foreground.rgb *= light.a;
	gl_FragColor = foreground;
}